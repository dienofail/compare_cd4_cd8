use warnings;
use strict;
use List::Util qw(sum);
use Statistics::Shannon;
sub mean {
    return sum(@_)/@_;
}

my $inputfile  = 'CD4_CD8_intersection.txt';
my $outputfile = 'CD4_CD8_intersection_analyzed.txt';
my %totalcd4hash;
my %totalcd8hash;
my $totalcount = 0;
my $cutoff = 300;
my @Karray;
my @Rarray;
my @Harray;
open( OUTPUT, ">$outputfile" );
close(OUTPUT);
open( INPUT, "$inputfile" );
my $line = <INPUT>;
while ( $line = <INPUT> )
{
	$line =~ s/\n$//;
	my @currentline = split( /\t/, $line );
	next if ( $#currentline < 45 );
	my $cd4index;
	my $cd8index;
	for my $i ( 0 .. $#currentline )
	{
		if ( $currentline[$i] eq 'CD4:' )
		{
			$cd4index = $i;
		}
		if ( $currentline[$i] eq 'CD8:' )
		{
			$cd8index = $i;
		}
	}
	my @key = splice( @currentline, 0, 3 );
	my $realkey = join( "\t", @key );
	print "Examining current key @key\n";
	print "$currentline[0] is what's left after removal of key\n";
	my @cd4array = splice( @currentline, 0, $cd8index - 3 );
	print "$currentline[0] is what's left after removal of cd4\n";
	shift(@cd4array);
	my @cd8array = @currentline;
	shift(@cd8array);
	print
"first element of cd4 is $cd4array[0] and first element of cd8 is $cd8array[0]\n";
	my %cd4hash;
	my %cd8hash;
	my $cd4counter;
	my $cd8counter;
	my %cd4compositionhash;
	my %cd8compositionhash;
	if ( $#cd4array > $cutoff && $#cd8array > $cutoff )
	{
		$totalcount ++;
		for my $i ( 0 .. $#cd4array )
		{
			my $current_val = $cd4array[$i];
			my @current_char_array = split( '', $current_val );
			for my $j ( 0 .. $#current_char_array )
			{
				$cd4counter++;
				$cd4hash{ $current_char_array[$j] } += 1;
				$cd4compositionhash{$j}{$current_char_array[$j]} += 1;
			}
		}
		for my $i ( 0 .. $#cd8array )
		{
			my $current_val = $cd8array[$i];
			my @current_char_array = split( '', $current_val );
			for my $j ( 0 .. $#current_char_array )
			{
				$cd8counter++;
				$cd8hash{ $current_char_array[$j] } += 1;
				$cd8compositionhash{$j}{$current_char_array[$j]} += 1;
			}
		}
		open( OUTPUT, ">>$outputfile" );
		print OUTPUT "Current_V_CDR3Length_J:\t$realkey\n";
		print OUTPUT "CD4\n";
		for my $key ( keys %cd4hash )
		{
			print OUTPUT "$key\t";
			my $value = $cd4hash{$key} / $cd4counter;
			$totalcd4hash{$key} += $value;
			print OUTPUT "$value\n";
		}
		print OUTPUT "CD8\n";
		for my $key ( keys %cd8hash )
		{
			print OUTPUT "$key\t";
			my $value = $cd8hash{$key} / $cd8counter;
			$totalcd8hash{$key} += $value;
			print OUTPUT "$value\n";
		}
		my $cd4averagecount = $cd4counter / $key[1];
		my $cd8averagecount = $cd8counter / $key[1];
		print OUTPUT "CD4_composition\tTotal_chars:$cd4counter\tChar_per_index:$cd4averagecount\n";
		for my $key1 (sort {$a <=> $b} keys %cd4compositionhash)
		{
			my %currenthash = %{$cd4compositionhash{$key1}};
			#%newhash = sort{$a cmp $b} keys %currenthash;
			my $currentshannon = compute_shannon(\%currenthash);
			my $currentsum = 0;
			print OUTPUT "$key1\t$currentshannon";
			foreach my $key (sort(keys %currenthash))
			{
				print OUTPUT "\t$key\t$currenthash{$key}";
				$currentsum += $currenthash{$key};
			}
			#print OUTPUT "\tRow_sum:$currentsum";
			print OUTPUT "\n";
		}
		print OUTPUT "CD8_composition\tTotal_chars:$cd8counter\tChar_per_index:$cd8averagecount\n";
		for my $key1 (sort {$a <=> $b} keys %cd8compositionhash)
		{
			my %currenthash = %{$cd8compositionhash{$key1}};
			#%newhash = sort{$a cmp $b} keys %currenthash;
			my $currentshannon = compute_shannon(\%currenthash);
			print OUTPUT "$key1\t$currentshannon";
			my $currentsum = 0;
			foreach my $key (sort(keys %currenthash))
			{
				print OUTPUT "\t$key\t$currenthash{$key}";
				$currentsum += $currenthash{$key};
			}
			#print OUTPUT "\tRow_sum:$currentsum";
			print OUTPUT "\n";
		}
		close(OUTPUT);
	}
}
close(INPUT);
for my $key (keys %totalcd4hash)
{
	my $cd4average = $totalcd4hash{$key} / $totalcount;
	my $cd8average = $totalcd8hash{$key} / $totalcount;
	my $cd4_cd8_ratio = $cd4average/$cd8average;
	print "$key\t$cd4_cd8_ratio\t$cd4average\t$cd8average\n";
	#print "$totalcount\n";
}
print "$totalcount\n";



sub compute_shannon
{
	my $hashref = shift;
	my %hash = %{$hashref};
	my $pop                    = Statistics::Shannon->new( \%hash );
	my $currentshannonindex    = $pop->index;
	return $currentshannonindex;
}