use warnings;
use strict;
use Cwd;

my $output = 'combined_new_CD4.txt';
my $output2 = 'combined_new_CD8.txt';
my $TargetDir = cwd; 

open(OUTPUT, ">$output");
open(OUTPUT2, ">$output2");
opendir (DIR, $TargetDir) or die $!;
while (my $file = readdir(DIR))
{
	print "$file\n";
	if ($file =~ m/CD4/)
	{
		open(INPUT, "$file");
		my $header = <INPUT>;
		while (my $line = <INPUT>)
		{
			my @currentline = split(/\s+/, $line);
			my $seq = $currentline[3];
			my $newseq = substr $seq, 1;
			chop($newseq);
			print OUTPUT "$newseq\n";
		}
		close(INPUT);
	}
	elsif ($file =~ m/CD8/)
	{
		open(INPUT, "$file");
		my $header = <INPUT>;
		while (my $line = <INPUT>)
		{
			my @currentline = split(/\s+/, $line);
			my $seq = $currentline[3];
			my $newseq = substr $seq, 1;
			chop($newseq);
			print OUTPUT2 "$newseq\n";
		}
		close(INPUT);	
	}
}
closedir(DIR);
close(OUTPUT);
close(OUTPUT2);