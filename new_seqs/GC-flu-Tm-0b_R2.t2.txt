Count	Percentage	CDR3 nucleotide sequence	CDR3 amino acid sequence	V segments	J segments	D segments	Last V nucleotide position	First D nucleotide position	Last D nucleotide position	First J nucleotide position	Good events	Total events	Good reads	Total reads
20	0.5405405405	TGCAGCGCCGCCGGGACTAGCGGGACCCGCTCCTACAATGAGCAGTTCTTC	CSAAGTSGTRSYNEQFF	TRBV29-1*01	TRBJ2-1*01	.	6	.	.	29	20	23	107	118
2	0.0540540541	TGTGCCAGTAGTATTAGAAGCGCCTACGAGCAGTACTTC	CASSIRSAYEQYF	TRBV19*01	TRBJ2-7*01	.	13	.	.	22	2	2	53	53
1	0.0270270270	TGTGCCACCACCTGGACAGTTCACACTGAAGCTTTCTTT	CATTWTVHTEAFF	TRBV15*01	TRBJ1-1*01	.	9	.	.	22	1	1	2	2
1	0.0270270270	TGTGACTGGAGTTCAGACGTGTGCTCTTCCGATCTACGTAGCTCAGTGGTATCAACGCAGAGTGTCTTTT	CDWSSDVCSSDct??acVAQWYQRRVSF	TRBV30*01	TRBJ1-5*01	.	11	.	.	66	1	1	2	2
1	0.0270270270	TGTGCCAGCAGGGTACAGGGCTCCTCTAATTCACCCCTCCACTTT	CASRVQGSSNSPLHF	TRBV12-3*01,TRBV12-4*01	TRBJ1-6*01	.	10	.	.	20	1	2	4	7
1	0.0270270270	TGTGCCAGTTCCCAGAGGAGCACCGGGGAGCTGTTTTTT	CASSQRSTGELFF	TRBV19*01	TRBJ2-2*01	.	8	.	.	17	1	1	17	17
1	0.0270270270	TGCAGCGTGCCGACGGGGGAAGGATACACAGATACGCAGTATTTT	CSVPTGEGYTDTQYF	TRBV29-1*01	TRBJ2-3*01	.	7	.	.	26	1	1	7	7
1	0.0270270270	TGTGCCACCAGCATTGGCAGGAACTATGGCTACACCTTC	CATSIGRNYGYTF	TRBV15*01	TRBJ1-2*01	.	12	.	.	21	1	1	2	2
1	0.0270270270	TGTGCCACCAGCAGAGGACAGGGGTCCTACGAGCAGTACTTC	CATSRGQGSYEQYF	TRBV15*01	TRBJ2-7*01	.	15	.	.	24	1	1	2	2
1	0.0270270270	TGTGCCAGCAGTTTCTCTAGCGCCGGGGAGCTGTTTTTT	CASSFSSAGELFF	TRBV12-3*01,TRBV12-4*01	TRBJ2-2*01	.	13	.	.	22	1	1	2	2
1	0.0270270270	TGTGCCAGCAGTGAAGCAGGGGAGGGCTCCTACGAGCAGTACTTC	CASSEAGEGSYEQYF	TRBV6-1*01	TRBJ2-7*01	.	16	.	.	26	1	2	2	5
1	0.0270270270	TGTGCCAGTAGTATAGATCGACAGGGTGGGGAGCTGTTTTTT	CASSIDRQGGELFF	TRBV19*01	TRBJ2-2*01	.	16	.	.	27	1	1	2	2
1	0.0270270270	TGTGCCAGTAGTAACGGGCAGATCTACAATGAGCAGTTCTTC	CASSNGQIYNEQFF	TRBV19*01	TRBJ2-1*01	.	12	.	.	23	1	1	43	43
1	0.0270270270	TGTGCCAGCAGCCCCCTAGTATTACGTGGCGGGGTCTACGAGCAGTACTTC	CASSPLVLRGGVYEQYF	TRBV7-9*01	TRBJ2-7*01	.	11	.	.	35	1	1	11	11
1	0.0270270270	TGTGCCAGCAGTGACCCGGGGAGGACCGGGGAGCTGTTTTTT	CASSDPGRTGELFF	TRBV2*01	TRBJ2-2*01	.	13	.	.	24	1	1	3	3
1	0.0270270270	TGCAGCGTTGGGGCTAGCGGGAGCCAAGAGACCCAGTACTTC	CSVGASGSQETQYF	TRBV29-1*01	TRBJ2-5*01	.	9	.	.	23	1	1	3	3
