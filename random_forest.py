#analysis algorithm for conversion of CDR3 peptide sequences to atchley factors
import numpy as np
import argparse
from sklearn import svm
from sklearn.tree import DecisionTreeClassifier
import random
import re
import csv
import collections as coll
from os import listdir
from os.path import isfile, join
import os
parser = argparse.ArgumentParser()
parser.add_argument("-CD8", "--CD8", type = str, default ='CD8_combined_output.txt',help = "Path to CD8 file")
parser.add_argument("-CD4", "--CD4", type = str, default = 'CD4_combined_output.txt', help = "Path to CD4 file")
parser.add_argument("-p", "--partition", type = float, default = 0.75, help = "Percentage of file to use as training set, a value from 0-1")
parser.add_argument("-l", "--length", type = int, default = 13, help = "Length of the amino acid to examinge")
parser.add_argument("-r", "--random", type = int, default = 5, help = "Number of times to randomize")
parser.add_argument("-n", "--nlimit", type = int, default = 10000, help = "Maximum number of CDR3s to analyze per run")
args = parser.parse_args()

aa=coll.defaultdict(int)
lookup=[[-0.591,-1.302,-0.733,1.570,-0.146],[-1.343,0.465,-0.862,-1.020,-0.255],[1.050,0.302,-3.656,-0.259,-3.242],[1.357,-1.453,1.477,0.113,-0.837],[-1.006,-0.590,1.891,-0.397,0.412],[-0.384,1.652,1.330,1.045,2.064],[0.336,-0.417,-1.673,-1.474,-0.078],[-1.239,-0.547,2.131,0.393,0.816],[1.831,-0.561,0.533,-0.277,1.648],[-1.019,-0.987,-1.505,1.266,-0.912],[-0.663,-1.524,2.219,-1.005,1.212],[0.945,0.828,1.299,-0.169,0.933],[0.189,2.081,-1.628,0.421,-1.392],[0.931,-0.179,-3.005,-0.503,-1.853],[1.538,-0.055,1.502,0.440,2.897],[-0.228,1.399,-4.760,0.670,-2.647],[-0.032,0.326,2.213,0.908,1.313],[-1.337,-0.279,-0.544,1.242,-1.262],[-0.595,0.009,0.672,-2.128,-0.184],[0.260,0.830,3.097,-0.838,1.512]]
aa['A']=0;aa['C']=1;aa['D']=2;aa['E']=3
aa['F']=4;aa['G']=5;aa['H']=6;aa['I']=7
aa['K']=8;aa['L']=9;aa['M']=10;aa['N']=11
aa['P']=12;aa['Q']=13;aa['R']=14;aa['S']=15
aa['T']=16;aa['V']=17;aa['W']=18;aa['Y']=19
p = re.compile("[\d\*]")


def process_file(file_name):
	global CD4_total_count, CD8_total_count
	file_list = list(csv.reader(open(file_name, 'rb'), delimiter = '\t'))
	total_CDR3s = [[i][0][1] for i in file_list if not p.match([i][0][1])]
	total_CDR3s = list(set(total_CDR3s))
	CDR3s = [[i][0][1] for i in file_list if not p.match([i][0][1]) and  len([i][0][1]) == args.length]
	CDR3s = list(set(CDR3s))
	temp_count = len(CDR3s)
	count_hash = {}
	for idx, val in enumerate(file_list):
		#print val
		if not p.match(val[1]):
			if val[1] in count_hash:
				count_hash[val[1]] += int(val[3])
			else:
				count_hash[val[1]] = int(val[3])


	#print "File name %s has %i unique CDR3s of %i length out of %i total unique sequences (%0.03f percent)"%(file_name, len(CDR3s), args.length, len(total_CDR3s), 100*len(CDR3s)/float(len(total_CDR3s)))
	# if len(CDR3s) > args.nlimit:
	# 	CDR3s = CDR3s[:args.nlimit]
	#print "File name %s processed with %i unique CDR3s of %i length out of %i total unique sequences (%0.02f percent)"%(file_name, len(CDR3s), args.length, len(total_CDR3s), len(CDR3s)/float(len(total_CDR3s)))
	return CDR3s, temp_count, count_hash



def process_file_LS(file_name):
	global CD4_total_count, CD8_total_count
	file_list = list(csv.reader(open(file_name, 'rb'), delimiter = '\t'))
	total_CDR3s = [[i][0][1] for i in file_list if not p.match([i][0][1])]
	total_CDR3s = list(set(total_CDR3s))
	CDR3s = [[i][0][1] for i in file_list if not p.match([i][0][1]) and  len([i][0][1]) == args.length]
	CDR3s = list(set(CDR3s))
	temp_count = len(CDR3s)
	count_hash = {}
	for idx, val in enumerate(file_list):
		#print val
		if not p.match(val[1]):
			if val[1] in count_hash:
				count_hash[val[1]] += int(val[3])
			else:
				count_hash[val[1]] = int(val[3])


	#print "File name %s has %i unique CDR3s of %i length out of %i total unique sequences (%0.03f percent)"%(file_name, len(CDR3s), args.length, len(total_CDR3s), 100*len(CDR3s)/float(len(total_CDR3s)))
	#print "File name %s processed with %i unique CDR3s of %i length out of %i total unique sequences (%0.02f percent)"%(file_name, len(CDR3s), args.length, len(total_CDR3s), len(CDR3s)/float(len(total_CDR3s)))
	return CDR3s, temp_count, count_hash


def encode(CDR3s, etype):
	_return = []
	for idx, val in enumerate(CDR3s):
		if etype == 'CD4':
			_return.append([val, 0])
		elif etype == 'CD8':
			_return.append([val, 1])
	return _return


def decode(CDR3s):
	#print(CDR3s)
	_return1 = []
	_return2 = []
	for idx, val in enumerate(CDR3s):
		#print(type(val))
		_return1.append(val[0])
		_return2.append(val[1])
	return _return1, _return2

def analyze(input_CD4, input_CD8, partition_N):
	#transform 
	global clf
	global overlap_array
	overlap_training = []
	overlap_training_tags = []
	overlap_testing = []
	overlap_testing_tags = []
	overlap_limit= int(round(partition_N*(len(overlap_array))))
	overlap_array = random.sample(overlap_array, len(overlap_array))
	overlap_len = len(overlap_array)
	print(overlap_len)
	for idx, val in enumerate(overlap_array[:overlap_limit]):
		overlap_training_tags.append(2)
		overlap_training.append(val)
	for idx, val in enumerate(overlap_array[overlap_limit:]):
		overlap_testing.append(val)
		overlap_testing_tags.append(2)
	overlap_testing = convert_array(overlap_testing)
	print(len(overlap_training))
	assert type(input_CD4) == list and type(input_CD8) == list, "Input to machine learning analysis method is not a list"
	assert type(input_CD4[0][0]) == str  and type(input_CD8[0][0]) == str, "We're not feeding strings to CDR3 analysis algorithm"
	CD4_limit = int(round(partition_N*(len(input_CD4))))
	CD8_limit = int(round(partition_N*(len(input_CD8))))
	#print(CD4_limit)
	CD4_randomized_list = random.sample(input_CD4, len(input_CD4))
	CD8_randomized_list = random.sample(input_CD8, len(input_CD8))
	#print(len(CD4_randomized_list[:CD4_limit]))
	CD4_training_set, CD4_training_tags = decode(CD4_randomized_list[:CD4_limit])
	CD4_training_set1 = convert_array(CD4_training_set)
	CD4_testing_set, CD4_testing_tags = decode(CD4_randomized_list[CD4_limit:])
	CD4_testing_set1 = convert_array(CD4_testing_set)
	CD8_training_set, CD8_training_tags = decode(CD8_randomized_list[:CD8_limit])
	CD8_training_set1 = convert_array(CD8_training_set)
	CD8_testing_set, CD8_testing_tags = decode(CD8_randomized_list[CD8_limit:])
	CD8_testing_set1 = convert_array(CD8_testing_set)
	total_training_set = convert_array(CD4_training_set + CD8_training_set + overlap_training)
	total_training_tags = CD4_training_tags + CD8_training_tags + overlap_training_tags
	clf = DecisionTreeClassifier(random_state = 0)
	clf.fit(total_training_set, total_training_tags)
	CD4_score_array = [0 for i in CD4_testing_set1]
	CD8_score_array = [0 for i in CD8_testing_set1]
	CD4_score = 0
	CD8_score = 0
	for idx, val in enumerate(CD4_score_array):
		CD4_score += val
	for idx, val in enumerate(CD8_score_array):
		CD8_score += val
	#let's score other stuff 
	return clf.score(CD4_testing_set1, CD4_testing_tags), clf.score(CD8_testing_set1, CD8_testing_tags), clf.score(overlap_testing, overlap_testing_tags), CD4_score/float(len(CD4_score_array)), CD8_score/float(len(CD8_score_array))

def analyze_old(input_file):
	global clf 
	training_set, testing_set = decode(input_file)
	training_set = convert_array(training_set)
	score = clf.score(training_set, testing_set)
	distance = [0 for i in training_set]
	average_distance = 0
	for idx, val in enumerate(distance):
		average_distance += val
	_final = average_distance/float(len(distance))
	return score, _final

def analyze_LS(input_file, LS_counts):
	global clf
	testing_set = convert_array(input_file) 
	score_array = clf.predict(testing_set)
	total_CD4, total_CD8, total_overlap = 0, 0, 0 
	count_CD4, count_CD8, count_overlap = 0, 0, 0
	average_distance_array = [0 for i in testing_set]
	total_distance_CD4, total_distance_CD8 = 0, 0 
	for idx, val in enumerate(score_array):
		if val == 0:
			total_CD4 += 1
			# print(input_file[idx])
			# print(LS_counts[str(input_file[idx])])
			count_CD4 += int(LS_counts[str(input_file[idx])])
			total_distance_CD4 += float(average_distance_array[idx])
		elif val == 1:
			total_CD8 += 1
			count_CD8 += int(LS_counts[str(input_file[idx])])
			total_distance_CD8 += float(average_distance_array[idx])
		elif val == 2:
			total_overlap += 1
			count_overlap += int(LS_counts[str(input_file[idx])])
	# print count_CD8
	# print count_CD4
	total = count_CD4 + count_CD8
	return total_CD4/float(len(score_array)), total_CD8/float(len(score_array)), total_overlap/float(len(score_array)), count_CD4/float(total), count_CD8/float(total), count_overlap/float(total), total_distance_CD4/float(len(score_array)), total_distance_CD8/float(len(score_array))

def convert_atchley1(z):
    global lookup
    return_array = []
    for idx, val in enumerate(z):
        if val not in aa:
            return 'null'
        lookup_array = lookup[aa[val]]
        for idx2, val2 in enumerate(lookup_array):
            return_array.append(float(val2))
    nparray = np.asarray(return_array, dtype =float)
    return nparray

def convert_atchley2(z):
	global lookup
	return_array = []
	for idx, val in enumerate(z):
		if val not in aa:
			return 'null'
		lookup_array = lookup[aa[val]]
		return_array.append(lookup_array[4])
	nparray = np.asarray(return_array, dtype = float)
	return nparray

def convert_array(x):
    norm_array = []
    for idx, val in enumerate(x):
        if len(val) == args.length:
            _push = convert_atchley1(val)
            if _push == 'null':
                continue
            norm_array.append(_push)
    np_array = np.asarray(norm_array, dtype=float)
    #print(np_array)
    return np_array

clf = None


#extract unique CDR3s
CD4_CDR3s, CD4_length1, _ = process_file(args.CD4)
CD8_CDR3s, CD8_length1, _ = process_file(args.CD8)


#find overlaps
hash1 = {}
hash2 = {}

for idx, val in enumerate(CD4_CDR3s):
	hash1[val] = 0

for idx, val in enumerate(CD8_CDR3s):
	hash2[val] = 0

overlap_array = []
for idx, val in hash1.iteritems():
	if idx in hash2:
		overlap_array.append(idx)


#encode CD4/CD8 CDR3s 
encoded_CD4 = encode(CD4_CDR3s, 'CD4')
encoded_CD8 = encode(CD8_CDR3s, 'CD8')

#randomize N times then perform SVM analysis 
CD4_score, CD8_score = 0, 0
CD4_scores = []
CD8_scores = []
CD4_dist, CD8_dist = 0, 0 
overlap_scores = []
for i in xrange(1, args.random):
	_CD4_score, _CD8_score, _overlap_score, _CD4_dist, _CD8_dist = analyze(encoded_CD4, encoded_CD8, args.partition)
	CD4_score += _CD4_score
	overlap_scores.append(_overlap_score)
	CD4_scores.append(_CD4_score)
	CD8_score += _CD8_score
	CD8_scores.append(_CD8_score)
	CD4_dist += _CD4_dist
	CD8_dist += _CD8_dist
	#print "After %i iterations, our score is is %0.02f for CD4 and %0.02f for CD8"%(int(i), CD4_score/float(i), CD8_score/float(i))
CD4_dist = CD4_dist/float(args.random)
CD8_dist = CD8_dist/float(args.random)
CD4_np_array = np.asarray(CD4_scores)
CD8_np_array = np.asarray(CD8_scores)
overlap_np_array = np.asarray(overlap_scores)
CD4_mean = np.mean(CD4_np_array)
CD8_mean = np.mean(CD8_np_array)
overlap_mean = np.mean(overlap_np_array)
CD4_std = np.std(CD4_np_array)
CD8_std = np.std(CD8_np_array)
overlap_std = np.std(overlap_np_array)
#print "With %i iterations done, final values are %0.02f for CD4 with %0.03f stdv and %0.02f for CD8 with %0.03f stdv"%(args.random, CD4_mean, CD4_std, CD8_mean, CD8_std)
print "%s\t%i\t%0.02f\t%0.02f\t%0.02f\t%0.02f\t%0.02f\t%0.02f\t%i\t%i\t%0.02f\t%0.02f"%('Original file', args.length, CD4_mean, CD8_mean, overlap_mean, CD4_std, CD8_std, overlap_std, CD4_length1, CD8_length1, CD4_dist, CD8_dist)




#hardcode new files 
CD4_O3_file = 'O3CAA_UNIQUE_OUTPUT.txt'
CD4_O4_file = 'O4a4-7_F6-8AA_UNIQUE_OUTPUT.txt'
CD8_O3_file = 'CD8_O3a8-7_F6-3AA_UNIQUE_OUTPUT.txt'
CD8_O4_file = 'CD8_O4a8-7e6_F7-8AA_UNIQUE_OUTPUT.txt'

CD4_O3_CDR3s, CD4_O3_length, _ = process_file(CD4_O3_file)
CD4_O4_CDR3s, CD4_O4_length, _  = process_file(CD4_O4_file)
CD8_O3_CDR3s, CD8_O3_length, _ = process_file(CD8_O3_file)
CD8_O4_CDR3s, CD8_O4_length, _ = process_file(CD8_O4_file)

CD4_Old_files = CD4_O3_CDR3s + CD4_O4_CDR3s
CD8_Old_files = CD8_O3_CDR3s + CD8_O4_CDR3s

encoded_CD4_old = encode(CD4_Old_files, 'CD4')
encoded_CD8_old = encode(CD8_Old_files, 'CD8')

CD4_score, CD4_distance = analyze_old(encoded_CD4_old)
CD8_score, CD8_distance = analyze_old(encoded_CD8_old)

print "%s\t%0.02f\t%0.02f\t%0.02f\t%0.02f"%('Combined_old', CD4_score, CD8_score, CD4_distance, CD8_distance)

#hardcode LS files 
LS_file_list = [f for f in listdir(os.getcwd()) if isfile(join(os.getcwd(), f)) and re.match("^L\d.C", f)]
for idx, val in enumerate(LS_file_list):
	_LS_CDR3s, _, LS_counts = process_file_LS(val)
	current_CD4, current_CD8, current_overlap, count_t_CD4, count_t_CD8, count_overlap, a, b = analyze_LS(_LS_CDR3s, LS_counts)
	print "%s\t%s\t%0.02f\t%0.02f\t%0.02f\t%i\t%0.02f\t%0.02f\t%0.02f\t%0.02f\t%0.02f"%('LS', val, current_CD4, current_CD8, current_overlap, len(_LS_CDR3s), count_t_CD4, count_t_CD8, count_overlap, a, b)

