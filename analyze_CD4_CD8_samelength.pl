#compute 

use warnings;
use strict;
my $inputfileone = 'CD4_total_output.txt';
my $inputfiletwo = 'CD8_total_output.txt';
my $output = 'KullbackLeibler_output.txt';
open(OUTPUT, ">$output");
close(OUTPUT);
my %cd4hash;
my %cd8hash;
my @code= qw/A C D E F G H I K L M N P Q R S T V W Y/;
open(INPUT, "$inputfileone");
while (my $line = <INPUT>)
{
	$line =~ s/\n$//;
	my @currentline = split(/\t/, $line);
	$cd4hash{$currentline[1]} += $currentline[3];
}
close(INPUT);


open(INPUT, "$inputfiletwo");
while (my $line = <INPUT>)
{
	$line =~ s/\n$//;
	my @currentline = split(/\t/, $line);
	$cd8hash{$currentline[1]} += $currentline[3];
}
close(INPUT);
for my $i (9..20)
{
main(\%cd4hash,\%cd8hash, $i);
}
sub main
{
	my $hashoneref = shift;
	my $hashtworef = shift;
	my $length = shift;
	my %hashone = %$hashoneref;
	my %hashtwo = %$hashtworef;
	my @hashonekeys = keys %hashone;
	my @hashtwokeys = keys %hashtwo;
	my %hashonecounts;
	my %hashtwocounts;
	my $cd4totalcdr3s;
	my $cd4totalAAs;
	my $cd8totalcdr3s;
	my $cd8totalAAs;

	for my $i (0..$length-1)
	{
		for my $j (0..$#code)
		{
			$hashonecounts{$i}{$code[$j]} = 0;
			$hashtwocounts{$i}{$code[$j]} = 0;
		}
	}
	for my $i (0..$#hashonekeys)
	{
		#$cd4totalcdr3s++;
		my $current_cdr3 = $hashonekeys[$i];
		my @current_array = split('', $current_cdr3);
		#print "@current_array\n";
		my $currentlength = scalar(@current_array);
		#print "$currentlength\n";
		if ($currentlength == $length)
		{
			$cd4totalcdr3s++;
			for my $i (0..$#current_array)
			{
				$cd4totalAAs++;
				$hashonecounts{$i}{$current_array[$i]}++;
			}
		}
	}

	for my $i (0..$#hashtwokeys)
	{
		#$cd8totalcdr3s++;
		my $current_cdr3 = $hashtwokeys[$i];
		my @current_array = split('', $current_cdr3);
		#print "@current_array\n";
		my $currentlength = scalar(@current_array);
		#print "$currentlength\n";
		if ($currentlength == $length)
		{
			$cd8totalcdr3s++;
			for my $i (0..$#current_array)
			{
				$cd8totalAAs++;
				$hashtwocounts{$i}{$current_array[$i]}++;
			}
		}
	}





	my $char_per_cd4_index = $cd4totalAAs / $length;
	my $char_per_cd8_index = $cd8totalAAs / $length;
	my %cd4probs;
	my %cd8probs;
 	open(OUTPUT, ">>$output");
	##let's print##
	print OUTPUT "Current_length: $length\n";
	print OUTPUT "CD4\n";
	print OUTPUT "Index";
	for my $i (0..$#code)
	{
		print OUTPUT "\t$code[$i]";
	}
	print OUTPUT "\n";
	for my $key1 (sort {$a <=> $b} keys %hashonecounts)
	{
		my %keyhash = %{$hashonecounts{$key1}};
		#my %nexthash = %$keyhash;
		print OUTPUT "$key1";
		for my $key2 (sort {$a cmp $b} keys %keyhash)
		{
			#print "$key2\t";
			#print "$key1 corresponds to $key2 corresponds to $keyhash{$key2}\n";
			my $toprintvalue = 100 * $keyhash{$key2} / $char_per_cd4_index;
			print OUTPUT "\t$toprintvalue";
			$cd4probs{$key1}{$key2} = $toprintvalue;
		}
		print OUTPUT "\n";
	}
	
	print OUTPUT "CD8\n";
	print OUTPUT "Index";
	for my $i (0..$#code)
	{
		print OUTPUT "\t$code[$i]";
	}
	print OUTPUT "\n";
	for my $key1 (sort {$a <=> $b} keys %hashtwocounts)
	{
		my %keyhash = %{$hashtwocounts{$key1}};
		#my %nexthash = %$keyhash;
		print OUTPUT "$key1";
		for my $key2 (sort {$a cmp $b} keys %keyhash)
		{
			#print "$key2\t"
			#print "$key1 corresponds to $key2 corresponds to $keyhash{$key2}\n";
			my $toprintvalue = 100 * $keyhash{$key2} / $char_per_cd8_index;
			print OUTPUT "\t$toprintvalue";
			$cd8probs{$key1}{$key2} = $toprintvalue;
		}
		print OUTPUT "\n";
	}


	print OUTPUT "CD4_CD8_difference\t$cd4totalcdr3s\t$cd8totalcdr3s\n";
	print OUTPUT "Index";
	for my $i (0..$#code)
	{
		print OUTPUT "\t$code[$i]";
	}
	print OUTPUT "\n";
	my @cd4keys = keys %cd4probs;
	my @cd8keys = keys %cd8probs;
	#print "@cd4keys\n";
	#print "@cd8keys\n";
	my %differencehash;
	##compute cd4-cd8 difference
	for my $key1 (sort {$a <=> $b} keys %cd4probs)
	{
		my %keyhash = %{$cd4probs{$key1}};
		#my %nexthash = %$keyhash;
		print OUTPUT "$key1";
		for my $key2 (sort {$a cmp $b} keys %keyhash)
		{
			#print "$key2\t"
			#print "$key1 corresponds to $key2 corresponds to $keyhash{$key2}\n";
			# my $toprintvalue = $keyhash{$key2} / $char_per_cd8_index;
			# print OUTPUT "\t$toprintvalue";
			# $cd8probs{$key1}{$key2} = $toprintvalue;
			my $toprintvalue = $cd4probs{$key1}{$key2} - $cd8probs{$key1}{$key2};
			print OUTPUT "\t$toprintvalue";
			$differencehash{$key1}{$key2} = $toprintvalue;
		}
		print OUTPUT "\n";
	}
	
	
	
	print OUTPUT "Weighted_CD4_CD8_difference\t$cd4totalcdr3s\t$cd8totalcdr3s\n";
	print OUTPUT "Index";
	for my $i (0..$#code)
	{
		print OUTPUT "\t$code[$i]";
	}
	print OUTPUT "\n";
	#my @cd4keys = keys %cd4probs;
	#my @cd8keys = keys %cd8probs;
	#print "@cd4keys\n";
	#print "@cd8keys\n";
	#my %differencehash;
	##compute cd4-cd8 difference
	for my $key1 (sort {$a <=> $b} keys %cd4probs)
	{
		my %keyhash = %{$cd4probs{$key1}};
		#my %nexthash = %$keyhash;
		print OUTPUT "$key1";
		for my $key2 (sort {$a cmp $b} keys %keyhash)
		{
			#print "$key2\t"
			#print "$key1 corresponds to $key2 corresponds to $keyhash{$key2}\n";
			# my $toprintvalue = $keyhash{$key2} / $char_per_cd8_index;
			# print OUTPUT "\t$toprintvalue";
			# $cd8probs{$key1}{$key2} = $toprintvalue;
			my $toprintvalue;
			if ($cd4probs{$key1}{$key2} == 0)
			{
				$toprintvalue  = 0;
			}
			else
			{	
			$toprintvalue = 100 * $differencehash{$key1}{$key2} / $cd4probs{$key1}{$key2};
			}
			print OUTPUT "\t$toprintvalue";
		}
		print OUTPUT "\n";
	}
	##
	##compute k-l divergence##
	# print OUTPUT "J-S_Divergence\n";
	# for my $key (sort {$a <=> $b} keys %hashonecounts)
	# {
	# 	next if ($key == 0 || $key == 1);
	# 	next if ($key == $length || $key == $length-1|| $key == $length-2 || $key == $length-3);
	# 	my $klvalue = jensen_shannon($hashonecounts{$key},$hashtwocounts{$key});
	# 	print OUTPUT "$key\t$klvalue\n";
	# }
	# print OUTPUT "\n";
	##print k-l divergence##





	print OUTPUT "\n\n\n";
	close(OUTPUT);


	



}


sub kl {
    my ($P, $Q, %opts) = @_;

    my $eps = 0.00001;

    $eps = $opts{epsilon} if exists $opts{epsilon};

    # Universe
    my $SU = {};
    $SU->{$_}++ for (keys %$P, keys %$Q);

    # | Universe - P |
    my $susp = scalar(keys %$SU) - scalar(keys %$P);

    # | Universe - Q |
    my $susq = scalar(keys %$SU) - scalar(keys %$Q);

    my $pc = $eps * ($susp/scalar(keys %$P));
    my $qc = $eps * ($susq/scalar(keys %$Q));

    my $Pline = sub {
        my $i = shift;
        return exists($P->{$i}) ? $P->{$i} - $pc : $eps;
    };
    my $Qline = sub {
        my $i = shift;
        return exists($Q->{$i}) ? $Q->{$i} - $pc : $eps;
    };

    my $kl = 0;
    for (keys %$SU) {
    	my $p = $Pline->($_);
    	my $q = $Qline->($_);
    	if ($p == 0 )
    	{
    		$p = 0.001;
    	}
    	elsif ($q == 0)
    	{
    		$q = 0.001;
    	}
        $kl += $p * log($p / $q);
    	#}
    }

    return $kl;
}


sub jensen_shannon {
    my ($P, $Q) = @_;           # $P=pointeur dist P, $Q=pointeur dist Q  
    my $log_2   = log(2) ;      # Logarithme base 2
    my %deja_vu = () ;          # Identifier les symboles de la distribution P
    map{ $deja_vu{$_}++ } (keys %$P) ;
    my @symbols = sort keys %deja_vu ;  # Symbols de la distribution P
        my $js      = 0 ;                   # init Jensen-Shannon
        foreach my $mot (@symbols) {        # Calcul de la divergence Jensen-Shannon
        $js     += $P->{$mot} * log( 2*$P->{$mot} / ( $P->{$mot} + $Q->{$mot} ) )/$log_2    # P Log ( P /(P+Q)/2 )
            +  $Q->{$mot} * log( 2*$Q->{$mot} / ( $P->{$mot} + $Q->{$mot} ) )/$log_2    # Q Log ( Q /(P+Q)/2 )
        }
        return $js/2 ;                      # retourner divergence Jensen-Shannon
}

# sub earthmover {
# 	my @bins1, @bins2;
# 	my $tot1, $tot2, $j, $carrying, $emd, $max;

# 	@bins1 = @{$_[0]};
# 	@bins2 = @{$_[1]};

# 	$tot1 = $tot2 = 0;
# 	for ($j = 3; $j <= $#bins1; $j++) {
# 		$bins1[$j] = $bins1[$j] ? length($bins1[$j]) : 0;
# 		$bins2[$j] = $bins2[$j] ? length($bins2[$j]) : 0;
# 		$tot1 += $bins1[$j];
# 		$tot2 += $bins2[$j];
# 	}

# 	$carrying = $emd = 0;
# 	for ($j = 3; $j <= $#bins1; $j++) {
# 		my $a = $bins1[$j]/$tot1;
# 		my $b = $bins2[$j]/$tot2;
# 		$carrying += $a - $b;
# 		$emd += abs($carrying);
# 	}

# 	$max = ($#bins1 - 4);
# 	return ($emd/$max) * 100 * 5;
# };


