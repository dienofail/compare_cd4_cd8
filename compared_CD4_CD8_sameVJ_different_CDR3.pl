use warnings;
use strict;
use Statistics::Shannon;
use List::Compare;

my %cd8hash;
my %cd4hash;
my $cutofflimit = 19;
my @cutoffarray;
my $fileone = 'CD4_combined_output.txt';
my $filetwo = 'CD8_combined_output.txt';

open(INPUT, "$fileone");
while (my $line = <INPUT>)
{
	$line =~ s/\n$//;
	my @currentline = split(/\s+/, $line);
	if ($#currentline > 1)
	{
	my $realkey = join( "\t", $currentline[0], $currentline[1], $currentline[2] );
	$cd4hash{$realkey} += $currentline[3];
	}
}
close(INPUT);

open(INPUT, "$filetwo");
while (my $line = <INPUT>)
{
	$line =~ s/\n$//;
	my @currentline = split(/\s+/, $line);
	if ($#currentline > 1)
	{
	my $realkey = join( "\t", $currentline[0], $currentline[1], $currentline[2] );
	$cd8hash{$realkey} += $currentline[3];
	}
}
close(INPUT);


##Find the similarities and then join
my @cd4keys = keys %cd4hash;
my @cd8keys = keys %cd8hash;
my %cd4shannoncounts;
my %cd8shannoncounts;
my %cd4lengthhash;
my %cd8lengthhash;
my %cd4totalcounts;
my %cd8totalcounts;
#for my $i (0..30)
#{
#	$cd4lengthhash{$i} = [];
#	$cd8lengthhash{$i} = [];
#}
#for my $i (0..$#cd4keys)
#{
#	for my $j (0..$#cd8keys)
#	{
#		my $currentcd4 = $cd4keys[$i];
#		my $currentcd8 = $cd8keys[$j];
#		my @arraycd4 = split(/\t/,$currentcd4);
#		my @arraycd8 = split(/\t/,$currentcd8);
#		my $currentcd4length = length($arraycd4[1]);
#		my $currentcd8length = length($arraycd8[1]);
#		my $cd4_key = join("\t", $arraycd4[0], $currentcd4length, $arraycd4[2]);
#		my $cd8_key = join("\t", $arraycd8[0], $currentcd8length, $arraycd8[2]);
#	}
#}

my %cd4answerhash;
for my $i (0..$#cd4keys)
{
	my $currentcd4 = $cd4keys[$i];
	my @arraycd4 = split(/\t/,$currentcd4);
	my $currentcd4length = length($arraycd4[1]);
	my $realkey = join("\t", $arraycd4[0], $currentcd4length, $arraycd4[2]);
	my @topusharray = @arraycd4;
	push(@{$cd4lengthhash{$currentcd4length}}, $arraycd4[1]);
	push(@{$cd4answerhash{$realkey}},\@topusharray);
}

my %cd8answerhash;
for my $i (0..$#cd8keys)
{
	my $currentcd8 = $cd8keys[$i];
	my @arraycd8 = split(/\t/,$currentcd8);
	my $currentcd8length = length($arraycd8[1]);
	my $realkey = join("\t", $arraycd8[0], $currentcd8length, $arraycd8[2]);
	my @topusharray = @arraycd8;
	push(@{$cd8answerhash{$realkey}},\@topusharray);
	push(@{$cd8lengthhash{$currentcd8length}}, $arraycd8[1]);
}

my @cd4answerkeys = keys %cd4answerhash;
my @cd8answerkeys = keys %cd8answerhash;

my $lc = List::Compare->new(\@cd4answerkeys,\@cd8answerkeys);
my @intersection = $lc->get_intersection();

my $output = 'CD4_CD8_intersection.txt';

open(OUTPUT, ">$output");
print OUTPUT "Intersection_key\tCD4_keys\tCD8_keys\n";
my $total_unique = 0;
my $cd4_unique = 0;
my $cd8_unique = 0;
for my $i (0..$#intersection)
{
	$total_unique++;
	print OUTPUT "$intersection[$i]\tCD4:";
	my @arrayrefs = @{$cd4answerhash{$intersection[$i]}};
	for my $i (0..$#arrayrefs)
	{
		$cd4_unique++;
		my @currentarray = @{$arrayrefs[$i]};
		print OUTPUT "\t$currentarray[1]";
	}
	print OUTPUT "\tCD8:";
	@arrayrefs = @{$cd8answerhash{$intersection[$i]}};
	for my $i (0..$#arrayrefs)
	{
		$cd8_unique++;
		my @currentarray = @{$arrayrefs[$i]};
		print OUTPUT "\t$currentarray[1]";
	}
	print OUTPUT "\n";
}
close(OUTPUT);
print "$total_unique\t$cd4_unique\t$cd8_unique\n";

###compute shannon indecies for every position

my %cd4shannonhash;
my %cd8shannonhash;
my @cd4less18array;
my @cd8less18array;
my @cd4more18array;
my @cd8more18array;
my $cd4less18count = 0;
my $cd4more18count = 0;
my $cd8less18count = 0;
my $cd8more18count = 0;
for my $i (0..$cutofflimit)
{
	push(@cd4less18array, {});
	push(@cd8less18array, {});
}

for my $i (0..35)
{
	push(@cd4more18array, {});
	push(@cd8more18array, {});
}

for my $key (keys %cd4lengthhash)
{
	$cd4shannonhash{$key} = [];
	for my $i (0..$key)
	{
		push(@{$cd4shannonhash{$key}}, {});
	}
}


for my $key (keys %cd4lengthhash)
{
	#I need to make a hash for each position. With 20 AAs. 
	my @currentarray = @{$cd4lengthhash{$key}};
	for my $i (0..$#currentarray)
	{
		$cd4shannoncounts{$key}++;
		$cd4totalcounts{$key}++;
		my @chararray = split('',$currentarray[$i]);
		for my $j (0..$#chararray)
		{
			$cd4shannonhash{$key}->[$j]->{$chararray[$j]} += 1;
			#print "For $key in cd4shannon hash, on the $j th element of charray, I am inputting $chararray[$j], for a final value output of $cd4shannonhash{$key}->[$j]->{$chararray[$j]}\n";
		}
		if ($key < $cutofflimit)
		{
			$cd4less18count++;
			for my $j (0..$#chararray)
			{
				$cd4less18array[$j]->{$chararray[$j]} += 1;
			}
		}
		if ($key >= $cutofflimit)
		{
			$cd4more18count++;
			print "$key\t$currentarray[$i]\n";
			#push(@cutoffarray, $currentarray[$i]);
			for my $j (0..$#chararray)
			{
				$cd4more18array[$j]->{$chararray[$j]} += 1;
			}
		}
	}
}

for my $key (keys %cd8lengthhash)
{
	$cd8shannonhash{$key} = [];
	for my $i (0..$key)
	{
		push(@{$cd8shannonhash{$key}}, {});
	}
}


for my $key (keys %cd8lengthhash)
{
	#I need to make a hash for each position. With 20 AAs. 
	my @currentarray = @{$cd8lengthhash{$key}};
	for my $i (0..$#currentarray)
	{
		my @chararray = split('',$currentarray[$i]);
		$cd8shannoncounts{$key}++;
		$cd8totalcounts{$key}++;
		for my $j (0..$#chararray)
		{
			$cd8shannonhash{$key}->[$j]->{$chararray[$j]} += 1;
			#print "For $key in cd8shannon hash, on the $j th element of charray, I am inputting $chararray[$j], for a final value output of $cd8shannonhash{$key}->[$j]->{$chararray[$j]}\n";
		}
		if ($key < $cutofflimit)
		{
			$cd8less18count++;
			for my $j (0..$#chararray)
			{
				$cd8less18array[$j]->{$chararray[$j]} += 1;
			}
		}
		if ($key >= $cutofflimit)
		{
			$cd8more18count++;
			print "$key\t$currentarray[$i]\n";
			push(@cutoffarray, $currentarray[$i]);
			for my $j (0..$#chararray)
			{
				$cd8more18array[$j]->{$chararray[$j]} += 1;
			}
		}
	}
}

#push empty arrays onto my shannonanswers hash
my %cd4shannonanswers;
my %cd8shannonanswers;

for my $key (keys %cd4shannonhash)
{
	$cd4shannonanswers{$key} = [];
}

for my $key (keys %cd8shannonhash)
{
	$cd8shannonanswers{$key} = [];
}

#compute shannonindex using my sub compute_shannon and then input into my shannonanswershash

for my $key (keys %cd4shannonhash)
{
	my @currentarray = @{$cd4shannonhash{$key}}; #currentarray returns an array of hashes refs;
	for my $i (0..$#currentarray)
	{
		my $currenthashref = $currentarray[$i];
		my $topushindex = compute_shannon($currenthashref);
		push(@{$cd4shannonanswers{$key}}, $topushindex);
	}
	
}

for my $key (keys %cd8shannonhash)
{
	my @currentarray = @{$cd8shannonhash{$key}}; #currentarray returns an array of hashes refs;
	for my $i (0..$#currentarray)
	{
		my $currenthashref = $currentarray[$i];
		my $topushindex = compute_shannon($currenthashref);
		push(@{$cd8shannonanswers{$key}}, $topushindex);
	}
}

my @cd4shannonkeys = keys %cd4shannonanswers;
my @cd8shannonkeys = keys %cd8shannonanswers;


my $shannonoutput = 'shannon_output.txt';
open (OUTPUT, ">$shannonoutput");
my $lc3 = List::Compare->new(\@cd4shannonkeys, \@cd8shannonkeys);
my @overlapping_shannon_keys = $lc3->get_intersection();
for my $i (0..$#overlapping_shannon_keys)
{
	print OUTPUT "Current_length\t$overlapping_shannon_keys[$i]\t\n";
	print OUTPUT "Keys:$cd4shannoncounts{$overlapping_shannon_keys[$i]}\tCD4:";
	my @cd4shannonarray = @{$cd4shannonanswers{$overlapping_shannon_keys[$i]}};
	for my $j (0..$#cd4shannonarray)
	{
		print OUTPUT "\t$cd4shannonarray[$j]";
	}
	print OUTPUT "\n";
	print OUTPUT "Keys:$cd8shannoncounts{$overlapping_shannon_keys[$i]}\tCD8:";
	my @cd8shannonarray = @{$cd8shannonanswers{$overlapping_shannon_keys[$i]}};
	for my $j (0..$#cd8shannonarray)
	{
		print OUTPUT "\t$cd8shannonarray[$j]";
	}
	print OUTPUT "\n";
}

close(OUTPUT);
#compute shannon index for combined cd4 and cd8 positions. 

my $cd4cd8shannonoutput = 'CD4_CD8_moreless18_output.txt';
open (OUTPUT, ">$cd4cd8shannonoutput");
my @cd4less18output = [];
my @cd4more18output = [];
my @cd8less18output = [];
my @cd8more18output = [];
print OUTPUT "Counts:$cd4less18count\tCD4_Less_$cutofflimit";
for my $i (0..$#cd4less18array)
{
	my $currenthashref = $cd4less18array[$i];
	my $currentshannon = compute_shannon($currenthashref);
	print OUTPUT "\t$currentshannon";
	push(@cd4less18output, $currentshannon);
}
print OUTPUT "\n";

print OUTPUT "Counts:$cd8less18count\tCD8_Less_$cutofflimit";
for my $i (0..$#cd8less18array)
{
	my $currenthashref = $cd8less18array[$i];
	my $currentshannon = compute_shannon($currenthashref);
	print OUTPUT "\t$currentshannon";
	push(@cd8less18output, $currentshannon);
}
print OUTPUT "\n";


print OUTPUT "Counts:$cd4more18count\tCD4_More_$cutofflimit";
for my $i (0..$#cd4more18array)
{
	my $currenthashref = $cd4more18array[$i];
	my $currentshannon = compute_shannon($currenthashref);
	print OUTPUT "\t$currentshannon";
	push(@cd4more18output, $currentshannon);
}
print OUTPUT "\n";

print OUTPUT "Counts:$cd8more18count\tCD8_More_$cutofflimit";
for my $i (0..$#cd8more18array)
{
	my $currenthashref = $cd8more18array[$i];
	my $currentshannon = compute_shannon($currenthashref);
	print OUTPUT "\t$currentshannon";
	push(@cd8more18output, $currentshannon);
}
print OUTPUT "\n";

close(OUTPUT);

my $countoutputs = 'CD4_CD8_total_counts_output.txt';
open (OUTPUT, ">$countoutputs");
print OUTPUT "CD4:\n";
for my $key (keys %cd4totalcounts)
{
	print OUTPUT "$key\t$cd4totalcounts{$key}\n";
}
print OUTPUT "CD8:\n";
for my $key (keys %cd8totalcounts)
{
	print OUTPUT "$key\t$cd8totalcounts{$key}\n";
}
close(OUTPUT);

my $cutoffoutput = 'This_is_too_hard.txt';


open(OUTPUT, ">$cutoffoutput");
for my $i (0..$#cutoffarray)
{
	print OUTPUT "$cutoffarray[$i]\n";
}
close(OUTPUT);
#compute shannon index for a certain position given a hashref
sub compute_shannon
{
	my $hashref = shift;
	my %hash = %{$hashref};
	my $pop                    = Statistics::Shannon->new( \%hash );
	my $currentshannonindex    = $pop->index;
	return $currentshannonindex;
}

