import numpy as np
import argparse
from sklearn import svm 
import random
import re
import csv
import collections as coll
from os import listdir
from os.path import isfile, join
import os
p = re.compile("[\d\*]")

def process_file(file_name):
	file_list = list(csv.reader(open(file_name, 'rb'), delimiter = '\t'))
	CDR3_hash = {}
	V_CDR3_J_hash = {}
	total_unique_CDR3 = 0
	total_unique_VJ = 0
	total_count = 0
	for idx, val in enumerate(file_list):
		#print val
		total_count += int(val[3])
		if not p.match(val[1]):
			if val[1] in CDR3_hash:
				CDR3_hash[val[1]] += int(val[3])
			else:
				CDR3_hash[val[1]] = int(val[3])

			V_CDR3_J_key = val[0]+val[1]+val[2]
			if V_CDR3_J_key in V_CDR3_J_hash:
				V_CDR3_J_hash[V_CDR3_J_key] += int(val[3])
			else:
				V_CDR3_J_hash[V_CDR3_J_key] = int(val[3])
	total_unique_CDR3 = len(CDR3_hash)
	total_unique_VJ = len(V_CDR3_J_hash)
	return CDR3_hash, V_CDR3_J_hash, total_unique_CDR3, total_unique_VJ, total_count


def find_overlap(hash1, hash2):
	unique_overlap = 0
	count_overlap_1 = 0
	count_overlap_2 = 0
	overlap_keys = []
	for key, val in hash1.iteritems():
		if key in hash2:
			unique_overlap += 1
			count_overlap_1 += val
			count_overlap_2 += hash2[key]
			overlap_keys.append(key)
	return unique_overlap, count_overlap_1, count_overlap_2, overlap_keys

def examine_LS_CDR3(file_in, overlap_list):
	overlap_set = {}
	for idx, val in enumerate(overlap_list): 
		overlap_set[val] = 1
	file_list = list(csv.reader(open(file_in, 'rb'), delimiter = '\t'))
	unique_overlap = 0
	count_overlap = 0
	total_count = 0
	total_unique = len(file_list)
	for idx, val in enumerate(file_list):
		total_count += int(val[3])
		if val[1] in overlap_set:
			unique_overlap += 1
			count_overlap += int(val[3])
	return unique_overlap, count_overlap, len(file_list), total_unique, total_count

def examine_LS_VJ(file_in, overlap_list):
	overlap_set = {}
	for idx, val in enumerate(overlap_list): 
		overlap_set[val] = 1
	file_list = list(csv.reader(open(file_in, 'rb'), delimiter = '\t'))
	unique_overlap = 0
	count_overlap = 0
	total_count = 0
	total_unique = len(file_list)
	for idx, val in enumerate(file_list):
		new_key = val[0] + val[1] + val[2]
		total_count += int(val[3])
		if new_key in overlap_set:
			unique_overlap += 1
			count_overlap += int(val[3])
			count_overlap += int(val[3])
	return unique_overlap, count_overlap, len(file_list), total_unique, total_count

CD4_file, CD8_file ='CD4_combined_output.txt', 'CD8_combined_output.txt'3

CD4_CDR3_hash, CD4_VJ_hash, CD4_unique_CDR3, CD4_unique_VJ, CD4_total_count = process_file(CD4_file)
CD8_CDR3_hash, CD8_VJ_hash, CD8_unique_CDR3, CD8_unique_VJ, CD8_total_count = process_file(CD8_file)
print "%s\t%i\t%i\t%i"%('CD4', CD4_unique_CDR3, CD4_unique_VJ, CD4_total_count)
print "%s\t%i\t%i\t%i"%('CD8', CD8_unique_CDR3, CD8_unique_VJ, CD8_total_count)


CDR3_overlap_unique, CDR3_overlap_count_1, CDR3_overlap_count_2, CDR3_overlap_array = find_overlap(CD4_CDR3_hash, CD8_CDR3_hash)
VJ_overlap_unique, VJ_overlap_count_1, VJ_overlap_count_2, VJ_overlap_array = find_overlap(CD4_VJ_hash, CD8_VJ_hash)
print("%i\t%i\t%i\t%i\t%i\t%i\n")%(CDR3_overlap_unique, CDR3_overlap_count_1, CDR3_overlap_count_2, VJ_overlap_unique, VJ_overlap_count_1, VJ_overlap_count_2) 

LS_file_list = [f for f in listdir(os.getcwd()) if isfile(join(os.getcwd(), f)) and re.match("^L\d.C", f)]
for idx, val in enumerate(LS_file_list):
	current_CD4, current_CD8, length, unique_count, total_count= examine_LS_CDR3(val, CDR3_overlap_array)
	print "%s\t%s\t%i\t%i\t%i\t%i\t%i\t%0.02f\t%0.02f"%('LS_CDR3', val, current_CD4, current_CD8, length, unique_count, total_count, 100*current_CD4/float(unique_count), 100*current_CD8/float(unique_count))
	current_CD4, current_CD8, length, unique_count, total_count= examine_LS_VJ(val, VJ_overlap_array)
	print "%s\t%s\t%i\t%i\t%i\t%i\t%i\t%0.02f\t%0.02f"%('LS_V_CDR3_J', val, current_CD4, current_CD8, length, unique_count, total_count, 100*current_CD4/float(unique_count), 100*current_CD8/float(unique_count))

