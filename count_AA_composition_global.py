# -*- coding: utf-8 -*-
"""
Created on Wed Nov 20 15:59:53 2013

@author: shiah
"""

import sys
sys.stdout.write('Hello')
import csv
import re
from decimal import *
from collections import defaultdict
import numpy as np
p = re.compile("\d+")
cd4file = '/home/shiah/repos/compare_cd4_cd8/CD4_total_output.txt'
cd8file = '/home/shiah/repos/compare_cd4_cd8/CD8_total_output.txt'
cd4k_array = []
cd4r_array = []
cd4h_array = []
cd8k_array = []
cd8r_array = []
cd8h_array = []

list_cd4 = list(csv.reader(open(cd4file, 'rb'), delimiter = '\t'))
list_cd8 = list(csv.reader(open(cd8file, 'rb'), delimiter = '\t'))

cd4_CDR3s = [[i][0][1] for i in list_cd4 if not p.match([i][0][1])]
cd8_CDR3s = [[i][0][1] for i in list_cd8 if not p.match([i][0][1])]

cd4_hash = defaultdict(int)
cd8_hash = defaultdict(int)
cd4_count = 0
cd8_count = 0
for idx,val in enumerate(cd4_CDR3s):
    current_char_array = list(val)
    current_length = len(current_char_array)
    #print "This is my current_length " + str(current_length)
    for idx2, val2 in enumerate(current_char_array):
        cd4_count += 1
        cd4_hash[val2] += 1
        #position_ratio = Decimal(idx2) / Decimal(current_length)
        #print "This is my ratio " + str(position_ratio) + " And my current idx " + str(idx2)
        if val2 == 'K':
            position_ratio = Decimal(idx2) / Decimal(current_length)
            cd4k_array.append(position_ratio)
        if val2 == 'R':
            position_ratio = Decimal(idx2) / Decimal(current_length)
            cd4r_array.append(position_ratio)
        if val2 == 'H':
            position_ratio = Decimal(idx2) / Decimal(current_length)
            cd4h_array.append(position_ratio)
    
        

for idx,val in enumerate(cd8_CDR3s):   
    current_char_array = list(val)
    current_length = len(current_char_array)
    for idx2, val2 in enumerate(current_char_array):
        cd8_count += 1    
        cd8_hash[val2] += 1
        #position_ratio = Decimal(idx2) / Decimal(current_length)
        if val2 == 'K':
            position_ratio = Decimal(idx2) / Decimal(current_length)
            cd8k_array.append(position_ratio)
        if val2 == 'R':
            position_ratio = Decimal(idx2) / Decimal(current_length)
            cd8r_array.append(position_ratio)
        if val2 == 'H':
            position_ratio = Decimal(idx2) / Decimal(current_length)
            cd8h_array.append(position_ratio)
        
for key in cd4_hash:
    current_value = cd4_hash[key]
    print_value = Decimal(current_value) / Decimal(cd4_count)    
    print "CD4:\t" + str(key) + "\t" + str(print_value) + "\n"    
for key in cd8_hash:
    current_value = cd8_hash[key]
    print_value = Decimal(current_value) / Decimal(cd8_count)    
    print "CD8:\t" + str(key) + "\t" + str(print_value) + "\n"
    
total_cd4k_array = Decimal(0)
counter = len(cd4k_array)
for idx, val in enumerate(cd4k_array):
    total_cd4k_array += val
result = Decimal(total_cd4k_array) / Decimal(counter)
print str(result)    

total_cd4r_array = Decimal(0)
counter = len(cd4r_array)
for idx, val in enumerate(cd4r_array):
    total_cd4r_array += val
result = Decimal(total_cd4r_array) / Decimal(counter)
print str(result)    

total_cd4h_array = Decimal(0)
counter = len(cd4h_array)
for idx, val in enumerate(cd4h_array):
    total_cd4h_array += val
result = Decimal(total_cd4h_array) / Decimal(counter)
print str(result)    

sys.stdout.write("CD4_K:\t" + str(np.mean(cd4k_array)))
sys.stdout.write("CD4_R:\t" + str(np.mean(cd4r_array)))
sys.stdout.write("CD4_H:\t" + str(np.mean(cd4h_array)))
sys.stdout.write("CD8_K:\t" + str(np.mean(cd8k_array)))
sys.stdout.write("CD8_R:\t" + str(np.mean(cd8r_array)))
sys.stdout.write("CD8_H:\t" + str(np.mean(cd8h_array)))
