import numpy as np
import csv
import re
import os
import sys
import matplotlib as mpl
import collections 
import matplotlib.pyplot as plt
from os import listdir
from os.path import isfile, join
import os
from sklearn import preprocessing   
import argparse
from numpy import linalg

from scipy.stats import ks_2samp
parser = argparse.ArgumentParser()
p = re.compile("[\d\*]")
parser = argparse.ArgumentParser()
parser.add_argument("-CD8", "--CD8", type = str, default ='CD8_combined_output.txt',help = "Path to CD8 file")
parser.add_argument("-CD4", "--CD4", type = str, default = 'CD4_combined_output.txt', help = "Path to CD4 file")
parser.add_argument("-l", "--length", type = int, default = 13, help = "Length of the amino acid to examinge")
args = parser.parse_args()
total_V_hash = {}
total_J_hash = {}

def find_CD4(file_name):
	global total_V_hash, total_J_hash
	total_hash = {}
	file_list = list(csv.reader(open(file_name, 'rb'), delimiter = '\t'))
	total_CDR3s = [[i][0][1] for i in file_list if not p.match([i][0][1])]
	total_CDR3s = list(set(total_CDR3s))
	CDR3s = [[i][0][1] for i in file_list if not p.match([i][0][1])]
	CDR3s = list(set(CDR3s))
	temp_count = len(CDR3s)
	for idx, val in enumerate(file_list):
	    #print val
	    if not p.match(val[1]):
	    	V_seg = str(val[1][:3])
	    	J_seg = str(val[1][-3:])
	    	if V_seg in total_V_hash:
	    		total_V_hash[V_seg][0] += 1
	    	else:
	    		total_V_hash[V_seg] = [1, 0]

	    	if J_seg in total_J_hash:
	    		total_J_hash[J_seg][0] += 1
	    	else:
	    		total_J_hash[J_seg] = [1, 0]

def find_CD8(file_name):
	global total_V_hash, total_J_hash
	file_list = list(csv.reader(open(file_name, 'rb'), delimiter = '\t'))
	total_CDR3s = [[i][0][1] for i in file_list if not p.match([i][0][1])]
	total_CDR3s = list(set(total_CDR3s))
	CDR3s = [[i][0][1] for i in file_list if not p.match([i][0][1])]
	CDR3s = list(set(CDR3s))
	temp_count = len(CDR3s)
	for idx, val in enumerate(file_list):
	    #print val
	    if not p.match(val[1]):
	    	V_seg = str(val[1][:3])
	    	J_seg = str(val[1][-3:])
	    	if V_seg in total_V_hash:
	    		total_V_hash[V_seg][1] += 1
	    	else:
	    		total_V_hash[V_seg] = [0, 1]

	    	if J_seg in total_J_hash:
	    		total_J_hash[J_seg][1] += 1
	    	else:
	    		total_J_hash[J_seg] = [0, 1]


def process_file(file_name):

    def f(x, y):
        return x / float(y) 

    global CD4_total_count, CD8_total_count
    _count_array = np.zeros((52, 13))
    _V_count = np.zeros(52)
    _J_count = np.zeros(13)
    file_list = list(csv.reader(open(file_name, 'rb'), delimiter = '\t'))
    total_CDR3s = [[i][0][1] for i in file_list if not p.match([i][0][1])]
    total_CDR3s = list(set(total_CDR3s))
    CDR3s = [[i][0][1] for i in file_list if not p.match([i][0][1])]
    CDR3s = list(set(CDR3s))
    temp_count = len(CDR3s)
    count_hash = {}
    V_dict = {}
    J_dict = {}
    _count = 0
    for idx, val in enumerate(file_list):
        #print val
        if not p.match(val[1]):
            _count_array[int(val[0])][int(val[2])] += 1
            _V_count[int(val[0])] += 1
            _J_count[int(val[2])] += 1
            _count += 1
            V_seg = str(val[1][:3])
            J_seg = str(val[1][-3:])
            if V_seg in V_dict:
                V_dict[V_seg] += 1
            else:
                V_dict[V_seg] = 1

            if J_seg in J_dict:
                J_dict[J_seg] += 1
            else:
                J_dict[J_seg] = 1
    t = np.vectorize(f)
    result_array = t(_count_array, _count)
    return _count_array, _count, V_dict, J_dict, _V_count, _J_count



def save(path, ext='png', close=True, verbose=True):
    """Save a figure from pyplot.

    Parameters
    ----------
    path : string
        The path (and filename, without the extension) to save the
        figure to.

    ext : string (default='png')
        The file extension. This must be supported by the active
        matplotlib backend (see matplotlib.backends module).  Most
        backends support 'png', 'pdf', 'ps', 'eps', and 'svg'.

    close : boolean (default=True)
        Whether to close the figure after saving.  If you want to save
        the figure multiple times (e.g., to multiple formats), you
        should NOT close it in between saves or you will have to
        re-plot it.

    verbose : boolean (default=True)
        Whether to print information about when and where the image
        has been saved.

    """

    # Extract the directory and filename from the given path
    directory = os.path.split(path)[0]
    filename = "%s.%s" % (os.path.split(path)[1], ext)
    if directory == '':
        directory = '.'

    # If the directory does not exist, create it
    if not os.path.exists(directory):
        os.makedirs(directory)

    # The final path to save to
    savepath = os.path.join(directory, filename)
    if verbose:
        print("Saving figure to '%s'..." % savepath),
    # Actually save the figure
    plt.savefig(savepath, dpi = 300)
    # Close it
    if close:
        plt.close()
    if verbose:
        print("Done")

find_CD4(args.CD4)
find_CD8(args.CD8)
CD4_V_ar = []
CD8_V_ar = []
CD4_J_ar = []
CD8_J_ar = []

f3 = open('3AA_dist.txt', 'w')
f3.write("V_AA\tCD4_count\tCD8_count\n\n")
for key, val in total_V_hash.iteritems():
	CD4_V_ar.append(val[0])
	CD8_V_ar.append(val[1])
	f3.write("%s\t%i\t%i\n"%(key, val[0], val[1]))
f3.write("\n\n\nJ_AA\tCD4_count\tCD8_count\n\n")
for key, val in total_J_hash.iteritems():
	CD4_J_ar.append(val[0])
	CD8_J_ar.append(val[1])
	f3.write("%s\t%i\t%i\n"%(key, val[0], val[1]))

CD4_V_ar = np.asarray(CD4_V_ar)
CD8_V_ar = np.asarray(CD8_V_ar)
CD4_J_ar = np.asarray(CD4_J_ar)
CD8_J_ar = np.asarray(CD8_J_ar)

CD4_V_ar = CD4_V_ar/sum(CD4_V_ar)
CD8_V_ar = CD8_V_ar/sum(CD8_V_ar)
CD4_J_ar = CD4_J_ar/sum(CD4_J_ar)
CD8_J_ar = CD8_J_ar/sum(CD8_J_ar)

_, p_value = ks_2samp(CD4_V_ar, CD8_V_ar)
print(p_value)
_, p_value = ks_2samp(CD4_J_ar, CD8_J_ar)
print(p_value)


CD4_, count_4, CD4_V_dict, CD4_J_dict, CD4_V_count, CD4_J_count = process_file(args.CD4)
CD4_ = CD4_ / count_4 
CD8_, count_8, CD8_V_dict, CD8_J_dict, CD8_V_count, CD8_J_count = process_file(args.CD8)
CD8_ = CD8_ / count_8 

CD4_V_count = CD4_V_count / sum(CD4_V_count)
CD4_J_count = CD4_J_count / sum(CD4_J_count)
CD8_V_count = CD8_V_count / sum(CD8_V_count)
CD8_J_count = CD8_J_count / sum(CD8_J_count)


f2 = open('V_J_stats.txt', 'w')
f2.write('CD4_V\n')
for idx, val in enumerate(CD4_V_count):
	f2.write("%i\t%0.03f\n"%(idx+1, val))
f2.write('\nCD8_V\n')
for idx, val in enumerate(CD8_V_count):
	f2.write("%i\t%0.03f\n"%(idx+1, val))
f2.write('\nCD4_J\n')
for idx, val in enumerate(CD4_J_count):
	f2.write("%i\t%0.03f\n"%(idx+1, val))
f2.write('\nCD8_J\n')
for idx, val in enumerate(CD8_J_count):
	f2.write("%i\t%0.03f\n"%(idx+1, val))

_, p_value = ks_2samp(CD4_V_count, CD8_V_count)
print(p_value)
_, p_value = ks_2samp(CD4_J_count, CD8_J_count)
print(p_value)


# CD4_V_array = []
# CD8_V_array = []
# CD4_J_array = []
# CD8_J_array = []
# for key, val in CD4_V_dict.iteritems():
#     if key in CD8_V_dict:
#         CD4_V_array.append(val)
#         CD8_V_array.append(CD8_V_dict[key])
       

# for key, val in CD4_J_dict.iteritems():
#     if key in CD8_J_dict:
#         CD4_J_array.append(val)
#         CD8_J_array.append(CD8_J_dict[key])

# CD4_V_array = np.asarray(CD4_V_array)

# CD8_V_array = np.asarray(CD8_V_array)

# CD4_J_array = np.asarray(CD4_J_array)

# CD8_J_array = np.asarray(CD8_J_array)



# CD4_V_array = CD4_V_array / sum(CD4_V_array)
# CD4_J_array = CD4_J_array / sum(CD4_J_array)
# CD8_V_array = CD8_V_array / sum(CD8_V_array)
# CD8_J_array = CD8_J_array / sum(CD8_J_array)


# _, _p1 = ks_2samp(CD4_V_array, CD8_V_array)
# _, _p2 = ks_2samp(CD4_J_array, CD8_J_array)
# print(_p1)
# print(_p2)
# fig, ax = plt.subplots()
# cmap1 = plt.cm.jet
# heatmap2 = ax.pcolormesh(CD4_, cmap=cmap1)
# cbar = plt.colorbar(heatmap2)
# ax.set_title('CD4 heatmap')
# save("CD4_plot", ext="png", close=False, verbose=True)


# fig, ax = plt.subplots()
# cmap1 = plt.cm.jet
# heatmap = ax.pcolormesh(CD8_, cmap=cmap1)
# cbar = plt.colorbar(heatmap)
# ax.set_title('CD8 heatmap')
# save("CD8_plot", ext="png", close=False, verbose=True)


# fig, ax = plt.subplots()
# cmap1 = plt.cm.jet
# heatmap32 = ax.pcolormesh(CD4_ - CD8_, cmap=cmap1)
# cbar = plt.colorbar(heatmap32) 
# ax.set_title('CD4 and CD8 difference heatmap')
# save("CD4_CD8_diffplot", ext="png", close=False, verbose=True)

# flat_CD4 = CD4_.flatten()
# flat_CD8 = CD8_.flatten()
# _, p_value = ks_2samp(flat_CD4, flat_CD8)
# print(p_value)

    #print "File name %s has %i unique CDR3s of %i length out of %i total unique sequences (%0.03f percent)"%(file_name, len(CDR3s), args.length, len(total_CDR3s), 100*len(CDR3s)/float(len(total_CDR3s)))
    # if len(CDR3s) > args.nlimit:
    #   CDR3s = CDR3s[:args.nlimit]
    #print "File name %s processed with %i unique CDR3s of %i length out of %i total unique sequences (%0.02f percent)"%(file_name, len(CDR3s), args.length, len(total_CDR3s), len(CDR3s)/float(len(total_CDR3s)))
    
