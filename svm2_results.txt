File name CD4_combined_output.txt has 18092 unique CDR3s of 15 length out of 218803 total unique sequences (8.269 percent)
File name CD8_combined_output.txt has 5177 unique CDR3s of 15 length out of 54868 total unique sequences (9.435 percent)
With 5 iterations done, final values are 0.96 for CD4 with 0.004 stdv and 0.97 for CD8 with 0.004 stdv
File name CD4_combined_output.txt has 8578 unique CDR3s of 16 length out of 218803 total unique sequences (3.920 percent)
File name CD8_combined_output.txt has 2113 unique CDR3s of 16 length out of 54868 total unique sequences (3.851 percent)
With 5 iterations done, final values are 0.99 for CD4 with 0.000 stdv and 0.98 for CD8 with 0.005 stdv
File name CD4_combined_output.txt has 3984 unique CDR3s of 17 length out of 218803 total unique sequences (1.821 percent)
File name CD8_combined_output.txt has 1871 unique CDR3s of 17 length out of 54868 total unique sequences (3.410 percent)
With 5 iterations done, final values are 0.99 for CD4 with 0.002 stdv and 0.99 for CD8 with 0.005 stdv
File name CD4_combined_output.txt has 1742 unique CDR3s of 18 length out of 218803 total unique sequences (0.796 percent)
File name CD8_combined_output.txt has 885 unique CDR3s of 18 length out of 54868 total unique sequences (1.613 percent)
With 5 iterations done, final values are 1.00 for CD4 with 0.002 stdv and 0.99 for CD8 with 0.002 stdv
