use warnings;
use strict;
use List::Util qw(sum);

sub mean {
    return sum(@_)/@_;
}
my $cd4kcount;
my $cd8kcount;
my $cd4rcount;
my $cd8rcount;
my $cd4hcount;
my $cd8hcount;
my $cd4totalcount;
my $cd8totalcount;
my $cd4totalpositive;
my $cd8totalpositive;
my @cd4karray;
my @cd4rarray;
my @cd4harray;
my @cd4parray;
my @cd8karray;
my @cd8rarray;
my @cd8harray;
my @cd8parray;

my $inputfile1 = 'CD4_total_output.txt';
my $inputfile2 = 'CD8_total_output.txt';

my $outputfile = 'Positive_AA_counts.txt';

open( OUTPUT, ">$outputfile" );
close(OUTPUT);
open( INPUT, "$inputfile1" );


while ( my $line = <INPUT> )
{
	$line =~ s/\n$//;
	$cd4totalcount++;
	my @currentline = split(/\t/, $line);
	my $currentAA = $currentline[1];

	if ($currentAA =~ m/K/)
	{
		$cd4kcount += 1;
		my @matches = $currentAA =~ m/K/g;
		my $count = scalar(@matches);
		push(@cd4karray, $count);

	}

	if ($currentAA =~ m/R/)
	{
		$cd4rcount += 1;
		my @matches = $currentAA =~ m/R/g;
		my $count = scalar(@matches);
		push(@cd4rarray, $count);
	}

	if ($currentAA =~ m/H/)
	{
		$cd4hcount += 1;
		my @matches = $currentAA =~ m/H/g;
		my $count = scalar(@matches);
		push(@cd4harray, $count);
	}
	if ($currentAA =~ m/[KRH]/)
	{
		$cd4totalpositive += 1;
		my @matches = $currentAA =~ m/[KRH]/g;
		my $count = scalar(@matches);
		push(@cd4parray, $count);
	}
}
close(INPUT);

open( INPUT, "$inputfile2" );


while ( my $line = <INPUT> )
{
	$line =~ s/\n$//;
	$cd8totalcount++;
	my @currentline = split(/\t/, $line);
	my $currentAA = $currentline[1];

	if ($currentAA =~ m/K/)
	{
		$cd8kcount += 1;
		my @matches = $currentAA =~ m/K/g;
		my $count = scalar(@matches);
		push(@cd8karray, $count);
	}

	if ($currentAA =~ m/R/)
	{
		$cd8rcount += 1;
		my @matches = $currentAA =~ m/R/g;
		my $count = scalar(@matches);
		push(@cd8rarray, $count);
	}

	if ($currentAA =~ m/H/)
	{
		$cd8hcount += 1;
		my @matches = $currentAA =~ m/H/g;
		my $count = scalar(@matches);
		push(@cd8harray, $count);
	}
	if ($currentAA =~ m/[KRH]/)
	{
		$cd8totalpositive += 1;
		my @matches = $currentAA =~ m/[KRH]/g;
		my $count = scalar(@matches);
		push(@cd8parray, $count);
	}
}
close(INPUT);

my $cd4kaverage = mean(@cd4karray);
my $cd4raverage = mean(@cd4rarray);
my $cd4haverage = mean(@cd4harray);
my $cd4paverage = mean(@cd4parray);
my $cd8kaverage = mean(@cd8karray);
my $cd8raverage = mean(@cd8rarray);
my $cd8haverage = mean(@cd8harray);
my $cd8paverage = mean(@cd8parray);

open(OUTPUT, ">$outputfile");
print OUTPUT "Type\tTotal_AA_Count\tK_positive\tR_positive\tH_positive\tTotal_positive\n";

print OUTPUT "CD4\t$cd4totalcount\t$cd4kcount\t$cd4rcount\t$cd4hcount\t$cd4totalpositive\n";

print OUTPUT "CD8\t$cd8totalcount\t$cd8kcount\t$cd8rcount\t$cd8hcount\t$cd8totalpositive\n";


print OUTPUT "Type\tAverage_K_count\tAverage_R_count\tAverage_H_count\tAverage_P_count\n";

print OUTPUT "CD4\t$cd4kaverage\t$cd4raverage\t$cd4haverage\t$cd4paverage\n";

print OUTPUT "CD8\t$cd8kaverage\t$cd8raverage\t$cd8haverage\t$cd8paverage\n";