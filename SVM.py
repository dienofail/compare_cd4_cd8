#!/usr/bin/env python
"""
Created on Mon Nov 18 22:35:23 2013

@author: AlvinDesktop
"""
import numpy as np
import sys
from time import time
from argparse import ArgumentParser
from sklearn import svm 
import re
import csv
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import preprocessing
import collections as coll
aa=coll.defaultdict(int)
lookup=[[-0.591,-1.302,-0.733,1.570,-0.146],[-1.343,0.465,-0.862,-1.020,-0.255],[1.050,0.302,-3.656,-0.259,-3.242],[1.357,-1.453,1.477,0.113,-0.837],[-1.006,-0.590,1.891,-0.397,0.412],[-0.384,1.652,1.330,1.045,2.064],[0.336,-0.417,-1.673,-1.474,-0.078],[-1.239,-0.547,2.131,0.393,0.816],[1.831,-0.561,0.533,-0.277,1.648],[-1.019,-0.987,-1.505,1.266,-0.912],[-0.663,-1.524,2.219,-1.005,1.212],[0.945,0.828,1.299,-0.169,0.933],[0.189,2.081,-1.628,0.421,-1.392],[0.931,-0.179,-3.005,-0.503,-1.853],[1.538,-0.055,1.502,0.440,2.897],[-0.228,1.399,-4.760,0.670,-2.647],[-0.032,0.326,2.213,0.908,1.313],[-1.337,-0.279,-0.544,1.242,-1.262],[-0.595,0.009,0.672,-2.128,-0.184],[0.260,0.830,3.097,-0.838,1.512]]

aa['A']=0;aa['C']=1;aa['D']=2;aa['E']=3
aa['F']=4;aa['G']=5;aa['H']=6;aa['I']=7
aa['K']=8;aa['L']=9;aa['M']=10;aa['N']=11
aa['P']=12;aa['Q']=13;aa['R']=14;aa['S']=15
aa['T']=16;aa['V']=17;aa['W']=18;aa['Y']=19


def convert_atchley(z):
    global lookup
    return_array = []
    for idx, val in enumerate(z):
        #print('currently look up '  + str(val))
        if val not in aa:
            return 'null'
        lookup_array = lookup[aa[val]]
        #print(lookup_array)
        for idx2, val2 in enumerate(lookup_array):
            return_array.append(float(val2))

    #print(return_array)
    nparray = np.asarray(return_array, dtype =float)
    return nparray

def convert_array(x):
    norm_array = []
    for idx, val in enumerate(x):
        if len(val) == 13:
            _push = convert_atchley(val)
            #print(_push)
            if _push == 'null':
                continue
            norm_array.append(_push)
    np_array = np.asarray(norm_array, dtype=float)
    #x_scaled = preprocessing.scale(np_array)
    print(np_array)
    return np_array

cd8file = 'CD8_combined_output.txt'
cd4file = 'CD4_combined_output.txt'
test_file = 'combined_new_CD8.txt'
p = re.compile("[\d\*]")
#from sklearn.datasets import fetch_20newsgroups
list_cd8 = list(csv.reader(open(cd8file, 'rb'), delimiter = '\t'))
# cd8limit = len(list_cd8) - 3002
print(len(list_cd8))
cd8limit = 35000
cd8_CDR3s = [[i][0][1] for i in list_cd8[:cd8limit] if not p.match([i][0][1]) and  len([i][0][1]) == 13]
cd8_CDR3s = set(cd8_CDR3s)
cd8_CDR3s = list(cd8_CDR3s)
cd8_tags = [0 for i in list_cd8[:cd8limit] if not p.match([i][0][1]) and len([i][0][1]) == 13]
cd8_testing = [[i][0][1] for i in list_cd8[-4000:] if not p.match([i][0][1]) and  len([i][0][1]) == 13]
cd8_testing_tags = [0 for i in list_cd8[-4000:] if not p.match([i][0][1]) and len([i][0][1]) == 13]
#print cd8_CDR3s

list_cd4 = list(csv.reader(open(cd4file, 'rb'), delimiter = '\t'))
print(len(list_cd4))
#cd4limit = len(list_cd4) - 3002
cd4limit = 35000
cd4_CDR3s = [[i][0][1] for i in list_cd4[:cd4limit] if not p.match([i][0][1]) and  len([i][0][1]) == 13]
cd4_CDR3s = set(cd4_CDR3s)
cd4_CDR3s = list(cd4_CDR3s)
#cd4_tags = [1 for i in list_cd4[:cd4limit] if not p.match([i][0][1]) and len([i][0][1]) == 13]
cd4_tags = [1 for i in cd4_CDR3s]
cd4_testing = [[i][0][1] for i in list_cd4[-4000:] if not p.match([i][0][1]) and len([i][0][1]) == 13]
cd4_testing_tags = [1 for i in list_cd4[-4000:] if not p.match([i][0][1]) and len([i][0][1]) == 13]

test_cd4 = open(test_file, 'r')
test_CDR3s=[]
test_tags = []
for val in test_cd4:
    #print(val)
    val.rstrip()
    val = re.sub("\n", '', val)
    if len(val) > 6 and len(val) == 13:
        if not p.search(val):
            test_CDR3s.append(val)
            test_tags.append(0)

full_cd4_cd8 = cd8_CDR3s + cd4_CDR3s
full_tags = cd8_tags + cd4_tags
print len(test_CDR3s)
#print cd4_CDR3s
#
#data_train = fetch_20newsgroups(subset='train')
#
#data_test = fetch_20newsgroups(subset='test')
#
#print data_train                              
#                                                  
print('data loaded')
vectorizer = CountVectorizer(analyzer ='char')
#analyze = vectorizer.build_analyzer()
#analyze(cd8_CDR3s[0:5])
#print vectorizer.get_feature_names()

combined_vec = convert_array(cd4_CDR3s + test_CDR3s)
combined_tags = cd4_tags + test_tags

clf2 = svm.SVC(kernel = 'poly', degree = 3)

full_vector =convert_array(full_cd4_cd8)
full_cd8_predictions_vector = convert_array(cd8_testing)
full_cd4_predictions_vector = convert_array(cd4_testing)
test_vector = convert_array(test_CDR3s)
test_tags = test_tags[:len(test_vector)]
#test_vector = convert_array(test_CDR3s)
#print cd8_vector
#superfull =convert_array(cd4_CDR3s + cd8_CDR3s + test_CDR3s)
#superfulltags = cd4_tags + cd8_tags + test_tags
#clf2.fit(superfull, superfulltags)
#print vectorizer.get_feature_names()
clf = svm.SVC(kernel = 'poly', degree = 3)
clf.fit(full_vector,full_tags)
cd8predictions = clf.score(full_cd8_predictions_vector,cd8_testing_tags)
print cd8predictions
cd4predictions = clf.score(full_cd4_predictions_vector,cd4_testing_tags)
print cd4predictions
testpredictions = clf.score(test_vector, test_tags)
print testpredictions
# testpredictions2 = clf2.score(combined_vec, combined_tags)
# print testpredictions2
#cd8totalcounter = 0
#cd8predtotal = 0
#for idx, val in cd8predictions:
#    cd8totalcounter += 1
#    cd8predtotal += val
#
#
#cd4totalcounter = 0
#cd4predtotal = 0
#for idx, val in cd8predictions:
#    cd4totalcounter += 1
#    cd4predtotal += val
#    
#import decimal as dec
#cd4_vector_nofit = vectorizer.fit(cd4_CDR3s)
#print cd4_vector_nofit
#print cd4_vector.toarray()[0]
#print type(cd4_vector.toarray())
#
#class CDR3:
#    def __init__(self, V_gene, CDR3, J_gene, Count):
#        self.V = V_gene
#        self.CDR3 = CDR3
#        self.J = J_gene
#        self.Count = Count

import os
import random
import string
import tempfile
import subprocess
 
def random_id(length=8):
    return ''.join(random.sample(string.ascii_letters + string.digits, length))
 
TEMPLATE_SERIAL = """
#####################################
#$ -S /bin/bash
#$ -cwd
#$ -N {name}
#$ -e {errfile}
#$ -o {logfile}
#$ -pe make {slots}
#####################################
echo "------------------------------------------------------------------------"
echo "Job started on" `date`
echo "------------------------------------------------------------------------"
{script}
echo "------------------------------------------------------------------------"
echo "Job ended on" `date`
echo "------------------------------------------------------------------------"
"""
 
def submit_python_code(code, name="job", logfile="output.$JOB_ID", errfile="error.$JOB_ID", cleanup=True, prefix="", slots=1):
    base = prefix + "submit_{0}".format(random_id())
    open(base + '.py', 'wb').write(code)
    script = "python " + base + ".py"
    open(base + '.qsub', 'wb').write(TEMPLATE_SERIAL.format(script=script, name=name, logfile=logfile, errfile=errfile, slots=slots))
    try:
        subprocess.call('qsub < ' + base + '.qsub', shell=True)
    finally:
        if cleanup:
            os.remove(base + '.qsub')
