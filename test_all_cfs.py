# -*- coding: utf-8 -*-
"""
Created on Tue Nov 19 14:38:57 2013

@author: shiah
"""

#!/usr/bin/env python
"""
Created on Mon Nov 18 22:35:23 2013

@author: AlvinDesktop
"""
import numpy as np
import sys
from time import time
from argparse import ArgumentParser
from sklearn import svm 
import re
import csv
from sklearn.feature_extraction.text import CountVectorizer
p = re.compile("\d+")

#from sklearn.datasets import fetch_20newsgroups
list_cd8 = list(csv.reader(open('CD8_combined_output.txt', 'rb'), delimiter = '\t'))
cd8limit = len(list_cd8) - 2002
cd8_CDR3s = [[i][0][1] for i in list_cd8[:cd8limit] if not p.match([i][0][1])]
cd8_tags = [0 for i in list_cd8[:cd8limit] if not p.match([i][0][1])]
cd8_testing = [[i][0][1] for i in list_cd8[-2000:] if not p.match([i][0][1])]
cd8_testing_tags = [0 for i in list_cd8[-2000:] if not p.match([i][0][1])]
#print cd8_CDR3s

list_cd4 = list(csv.reader(open('CD4_combined_output.txt', 'rb'), delimiter = '\t'))
cd4limit = len(list_cd4) - 5002
cd4_CDR3s = [[i][0][1] for i in list_cd4[:cd4limit] if not p.match([i][0][1])]
cd4_tags = [1 for i in list_cd4[:cd4limit] if not p.match([i][0][1])]
cd4_testing = [[i][0][1] for i in list_cd4[-2000:] if not p.match([i][0][1])]
cd4_testing_tags = [1 for i in list_cd4[-2000:] if not p.match([i][0][1])]



full_cd4_cd8 = cd8_CDR3s + cd4_CDR3s
full_tags = cd8_tags + cd4_tags

#print cd4_CDR3s
#
#data_train = fetch_20newsgroups(subset='train')
#
#data_test = fetch_20newsgroups(subset='test')
#
#print data_train                              
#                             
                               
                               
                            
print('data loaded')
vectorizer = CountVectorizer(analyzer ='char')
#analyze = vectorizer.build_analyzer()
#analyze(cd8_CDR3s[0:5])
#print vectorizer.get_feature_names()

from sklearn import cross_validation
from sklearn import tree
full_vector = vectorizer.fit_transform(full_cd4_cd8)
full_cd8_predictions_vector = vectorizer.fit_transform(cd8_testing)
full_cd4_predictions_vector = vectorizer.fit_transform(cd4_testing)
#print cd8_vector
print vectorizer.get_feature_names()
clf = tree.DecisionTreeClassifier()
clf.fit(full_vector.toarray(),full_tags)
cd8predictions = clf.score(full_cd8_predictions_vector.toarray(),cd8_testing_tags)
cd4predictions = clf.score(full_cd4_predictions_vector.toarray(),cd4_testing_tags)
print cd8predictions
print cd4predictions

from sklearn.externals.six import StringIO
with open('Tree.dot', 'w') as f:
    f = tree.export_graphviz(clf, out_file=f)

def kl(p, q):
    p = np.asarray(p, dtype=np.float)
    q = np.asarray(q, dtype=np.float)
    return np.sum(np.where(p != 0,(p-q) * np.log10(p / q), 0))

#cd8totalcounter = 0
#cd8predtotal = 0
#for idx, val in cd8predictions:
#    cd8totalcounter += 1
#    cd8predtotal += val
#
#
#cd4totalcounter = 0
#cd4predtotal = 0
#for idx, val in cd8predictions:
#    cd4totalcounter += 1
#    cd4predtotal += val
#    
#import decimal as dec


#cd4_vector_nofit = vectorizer.fit(cd4_CDR3s)
#print cd4_vector_nofit



#print cd4_vector.toarray()[0]
#print type(cd4_vector.toarray())
#
#class CDR3:
#    def __init__(self, V_gene, CDR3, J_gene, Count):
#        self.V = V_gene
#        self.CDR3 = CDR3
#        self.J = J_gene
#        self.Count = Count
