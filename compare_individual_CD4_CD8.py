# -*- coding: utf-8 -*-
"""
Created on Tue Dec 03 13:36:09 2013

@author: shiah
"""


def cd4cd8analysis(cd4file, cd8file):
    import csv
    import re
    list_of_AAs = "A C D E F G H I K L M N P Q R S T V W Y".split()
    p = re.compile("\d+")
    ##CD4 and CD8 first##
    list_cd4 = list(csv.reader(open(cd4file, 'r'), delimiter='\t'))
    list_cd8 = list(csv.reader(open(cd8file, 'r'), delimiter='\t'))
    cd4_dict = dict(zip(list_of_AAs, [0]*len(list_of_AAs)))
    cd8_dict = dict(zip(list_of_AAs, [0]*len(list_of_AAs)))
    cd4_count = 0
    cd8_count = 0
    cd4_AA_count = 0
    cd8_AA_count = 0
    cd4_CDR3s = [[i][0][1] for i in list_cd4 if not p.match([i][0][1])]
    cd8_CDR3s = [[i][0][1] for i in list_cd8 if not p.match([i][0][1])]
    global cd4AAhash
    global cd8AAhash
    for idx, val in enumerate(cd4_CDR3s):
        current_array = list(val)
        cd4_count += 1
        for idx, val in enumerate(current_array):
            cd4_dict[val] += 1
            cd4_AA_count += 1
    for idx, val in enumerate(cd8_CDR3s):
        cd8_count += 1
        current_array = list(val)
        for idx, val in enumerate(current_array):
            cd8_dict[val] += 1
            cd8_AA_count += 1
    cd4_AA_composition_list = []
    cd8_AA_composition_list = []
    for key in cd4_dict:
        current_val = float(cd4_dict[key]) / (cd4_AA_count)
        cd4AAhash[key].append(current_val)
        cd4_AA_composition_list.append(current_val)
    for key in cd8_dict:
        current_val = float(cd8_dict[key]) / (cd8_AA_count)
        cd8AAhash[key].append(current_val)
        cd8_AA_composition_list.append(current_val)
    cd4_Numpy_array = np.asarray(cd4_AA_composition_list)
    cd8_Numpy_array = np.asarray(cd8_AA_composition_list)
    cd4_cd8_covariance = np.cov(cd4_Numpy_array, cd8_Numpy_array)[0][1]
    cd4_cd8_coeff = np.corrcoef(cd4_Numpy_array, cd8_Numpy_array)[0][1]
    sys.stdout.write('My covariance for ' + str(cd4file) + ' and ' + str(cd8file) + ' is ' + str(cd4_cd8_covariance) + ' And my correlation coefficient is ' + str(cd4_cd8_coeff) + "\n")
    return


def cd4analysis(cd4file):
    import csv
    import re
    import random
    list_of_AAs = "A C D E F G H I K L M N P Q R S T V W Y".split()
    p = re.compile("\d+")
    ##CD4 and CD8 first##
    list_cd4 = list(csv.reader(open(cd4file, 'rb'), delimiter='\t'))
    part1_dict = dict(zip(list_of_AAs, [0]*len(list_of_AAs)))
    part2_dict = dict(zip(list_of_AAs, [0]*len(list_of_AAs)))
    cd4_CDR3s = [[i][0][1] for i in list_cd4 if not p.match([i][0][1])]
    random.shuffle(cd4_CDR3s)
    chunk_length = len(cd4_CDR3s) // 2
    part1 = cd4_CDR3s[:chunk_length]
    part2 = cd4_CDR3s[chunk_length:]
    part1_AA_count = 0
    part2_AA_count = 0
    part1_AA_composition_list = []
    part2_AA_composition_list = []
    global part1hash
    global part2hash
    for idx, val in enumerate(part1):
        current_array = list(val)
        for idx, val in enumerate(current_array):
            part1_dict[val] += 1
            part1_AA_count += 1
    for idx, val in enumerate(part2):
        current_array = list(val)
        for idx, val in enumerate(current_array):
            part2_dict[val] += 1
            part2_AA_count += 1
    for key in part1_dict:
        current_val = float(part1_dict[key]) / float(part1_AA_count)
        part1hash[key].append(current_val)
        part1_AA_composition_list.append(current_val)
    for key in part2_dict:
        current_val = float(part2_dict[key]) / float(part2_AA_count)
        part2hash[key].append(current_val)
        part2_AA_composition_list.append(current_val)
    part1_Numpy_array = np.asarray(part1_AA_composition_list)
    part2_Numpy_array = np.asarray(part2_AA_composition_list)
    part1_part2_covariance = np.cov(part1_Numpy_array, part2_Numpy_array)[0][1]
    part1_part2_coeff = np.corrcoef(part1_Numpy_array, part2_Numpy_array)[0][1]
    sys.stdout.write('My within cd4 covariance for ' + str(cd4file) + ' is ' + str(part1_part2_covariance) + ' And my correlation coefficient is ' + str(part1_part2_coeff) + "\n")


def cd8analysis(cd8file):
    import csv
    import re
    import random
    list_of_AAs = "A C D E F G H I K L M N P Q R S T V W Y".split()
    p = re.compile("\d+")
    ##CD4 and CD8 first##
    list_cd8 = list(csv.reader(open(cd8file, 'rb'), delimiter='\t'))
    part1_dict = dict(zip(list_of_AAs, [0]*len(list_of_AAs)))
    part2_dict = dict(zip(list_of_AAs, [0]*len(list_of_AAs)))
    cd8_CDR3s = [[i][0][1] for i in list_cd8 if not p.match([i][0][1])]
    random.shuffle(cd8_CDR3s)
    chunk_length = len(cd8_CDR3s) // 2
    part1 = cd8_CDR3s[:chunk_length]
    part2 = cd8_CDR3s[chunk_length:]
    part1_AA_count = 0
    part2_AA_count = 0
    part1_AA_composition_list = []
    part2_AA_composition_list = []
    global cd8part1hash
    global cd8part2hash
    for idx, val in enumerate(part1):
        current_array = list(val)
        for idx, val in enumerate(current_array):
            part1_dict[val] += 1
            part1_AA_count += 1
    for idx, val in enumerate(part2):
        current_array = list(val)
        for idx, val in enumerate(current_array):
            part2_dict[val] += 1
            part2_AA_count += 1
    for key in part1_dict:
        current_val = float(part1_dict[key]) / float(part1_AA_count)
        cd8part1hash[key].append(current_val)
        part1_AA_composition_list.append(current_val)
    for key in part2_dict:
        current_val = float(part2_dict[key]) / float(part2_AA_count)
        cd8part2hash[key].append(current_val)
        part2_AA_composition_list.append(current_val)
    part1_Numpy_array = np.asarray(part1_AA_composition_list)
    part2_Numpy_array = np.asarray(part2_AA_composition_list)
    part1_part2_covariance = np.cov(part1_Numpy_array, part2_Numpy_array)[0][1]
    part1_part2_coeff = np.corrcoef(part1_Numpy_array, part2_Numpy_array)[0][1]
    sys.stdout.write('My within cd8 covariance for ' + str(cd8file) + ' is ' + str(part1_part2_covariance) + ' And my correlation coefficient is ' + str(part1_part2_coeff) + "\n")


def chunk(xs, n):
    import random
    ys = list(xs)
    random.shuffle(ys)
    chunk_length = len(ys) // n
    needs_extra = len(ys) % n
    start = 0
    for i in xrange(n):
        if i < needs_extra:
            end = start + chunk_length + 1
        else:
            end = start + chunk_length
        yield ys[start:end]
        start = end


import subprocess
import os
import sys
import numpy as np
from scipy import stats
list_of_AAs = "A C D E F G H I K L M N P Q R S T V W Y".split()

CD4_list = "CD4_M1.txt CD4_M2.txt CD4_M3.txt CD4_O1.txt CD4_O2.txt CD4_Y1.txt CD4_Y2.txt CD4_Y3.txt".split()
CD8_list = "CD8_M1.txt CD8_M2.txt CD8_M3.txt CD8_O1.txt CD8_O2.txt CD8_Y1.txt CD8_Y2.txt CD8_Y3.txt".split()
cd4AAhash = {k: [] for k in list_of_AAs}
cd8AAhash = {k: [] for k in list_of_AAs}
part1hash = {k: [] for k in list_of_AAs}
part2hash = {k: [] for k in list_of_AAs}
cd8part1hash = {k: [] for k in list_of_AAs}
cd8part2hash = {k: [] for k in list_of_AAs}

for idx,val in enumerate(CD4_list):
    cd4cd8analysis(val,CD8_list[idx])
    cd4analysis(val)
f1 = open('CD4_CD8_individual_comparison_output_new.txt', 'w')
for key in cd4AAhash:
    sys.stdout.write("current key is " + str(key) + "\n")
    f1.write(str(key) + "\n")
    cd4_total_numpy_array = np.asarray(cd4AAhash[key])
    f1.write('CD4_usage' + "\t"+ str("\t".join(map(str,cd4_total_numpy_array))) + "\n")
    cd8_total_numpy_array = np.asarray(cd8AAhash[key])
    f1.write('CD8_usage' + "\t" + str("\t".join(map(str,cd8_total_numpy_array))) + "\n")
    part1_total_numpy_array = np.asarray(part1hash[key])
    f1.write('CD4_Part1_usage' + "\t" + str("\t".join(map(str,part1_total_numpy_array))) + "\n")
    part2_total_numpy_array = np.asarray(part2hash[key])
    f1.write('CD4_Part2_usage' + "\t" + str("\t".join(map(str,part2_total_numpy_array))) + "\n")
    cd8part1_total_numpy_array = np.asarray(cd8part1hash[key])
    f1.write('CD8_Part1_usage' + "\t" + str("\t".join(map(str,cd8part1_total_numpy_array))) + "\n")
    cd8part2_total_numpy_array = np.asarray(cd8part2hash[key])
    f1.write('CD8_Part2_usage' + "\t" + str("\t".join(map(str,cd8part2_total_numpy_array))) + "\n")
    ###calculate covariance/coefficients####
    cd4_cd8_covariance = np.cov(cd4_total_numpy_array, cd8_total_numpy_array)[0][1]
    cd4_cd8_coeff = np.corrcoef(cd4_total_numpy_array, cd8_total_numpy_array)[0][1]
    cd4_cd8_t_test = stats.ttest_rel(cd4_total_numpy_array, cd8_total_numpy_array)[1]
    cd4_cd8_kw = stats.mstats.kruskalwallis(cd4_total_numpy_array, cd8_total_numpy_array)[1]
    cd4_cd8_MU = stats.mannwhitneyu(cd4_total_numpy_array, cd8_total_numpy_array)[1]
    f1.write('CD4_CD8_Covariance,Coeff,P-value,KW_pvalue,MU_pvalue' + "\t" + str(cd4_cd8_covariance) + "\t" + str(cd4_cd8_coeff) + "\t" + str(cd4_cd8_t_test) + "\t" + str(cd4_cd8_kw) + "\t" + str(cd4_cd8_MU) + "\n")
    part1_part2_covariance = np.cov(part1_total_numpy_array, part2_total_numpy_array)[0][1]
    part1_part2_coeff = np.corrcoef(part1_total_numpy_array, part2_total_numpy_array)[0][1]
    part1_part2_t_test = stats.ttest_rel(part1_total_numpy_array, part2_total_numpy_array)[1]
    part1_part2_kw = stats.mstats.kruskalwallis(part1_total_numpy_array, part2_total_numpy_array)[1]
    part1_part2_MU = stats.mannwhitneyu(part1_total_numpy_array, part2_total_numpy_array)[1]
    f1.write('CD4_within_Covariance,Coeff,P-value,KW_pvalue,MU_pvalue' + "\t" + str(part1_part2_covariance) + "\t" + str(part1_part2_coeff) + "\t" + str(part1_part2_t_test) + "\t" + str(part1_part2_kw) + "\t" + str(part1_part2_MU) + "\n")
    cd8part1_part2_covariance = np.cov(cd8part1_total_numpy_array, cd8part2_total_numpy_array)[0][1]
    cd8part1_part2_coeff = np.corrcoef(cd8part1_total_numpy_array, cd8part2_total_numpy_array)[0][1]
    cd8part1_part2_t_test = stats.ttest_rel(cd8part1_total_numpy_array, cd8part2_total_numpy_array)[1]
    cd8part1_part2_kw = stats.mstats.kruskalwallis(cd8part1_total_numpy_array, cd8part2_total_numpy_array)[1]
    cd8part1_part2_MU = stats.mannwhitneyu(cd8part1_total_numpy_array, cd8part2_total_numpy_array)[1]
    f1.write('CD8_within_Covariance,Coeff,P-value,KW_pvalue,MU_pvalue' + "\t" + str(cd8part1_part2_covariance) + "\t" + str(cd8part1_part2_coeff) + "\t" + str(cd8part1_part2_t_test) + "\t" + str(cd8part1_part2_kw) + "\t" + str(cd8part1_part2_MU) + "\n")